site.directive('contactForm', ['getLang', 'wpjson', '$window', function (getLang, wpjson, $window ) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            $('.email-inp').on('keyup', function () {
                if(emailValid( $(this).val())) $(this).css('border-color', '#08708A');
                else $(this).css('border-color', '#d73a31');
            });

            $('.name-inp, .subj-inp, .msg-inp').on('keyup', function () {
                if(textValid( $(this).val())) $(this).css('border-color', '#08708A');
                else $(this).css('border-color', '#d73a31');
            });

            scope.formValid = function () {
                scope.doneMSG = '';
                if(!textValid($('.name-inp').val())){
                    $('.name-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['trukstaVardo'];
                    return false;
                }
                if(!emailValid($('.email-inp').val())) {
                    $('.email-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['neteisingasElpastas'];
                    return false;
                }

                if(!textValid($('.subj-inp').val())){
                    ('.subj-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['trukstaTemos'];
                    return false;
                }

                if(!textValid($('.msg-inp').val())){
                    $('.msg-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['trukstaZinutes'];
                    return false;
                }
                scope.errorMSG = null;
                grecaptcha.execute();
            };
            scope.submitMsg = function (token) {
                wpjson.sendEmail({name: $('.name-inp').val(), email: $('.email-inp').val(), subject: $('.subj-inp').val(), msg: $('.msg-inp').val()}).then(function (data) {
                    if(data){
                        $('.name-inp').val('');
                        $('.email-inp').val('');
                        $('.subj-inp').val('');
                        $('.msg-inp').val('');
                        scope.errorMSG = '';
                        scope.doneMSG = translations['sekmingaiNusiusta'];
                    } else {
                        scope.errorMSG = translations['nesekmingaiNusiusta'];
                    }

                }, function (err) {
                    console.log(err);
                    scope.errorMSG = translations['nesekmingaiNusiusta'];
                });
            };
            $window.submitMsg = scope.submitMsg;

            function textValid(value) {
                return value.trim().length > 0;
            }
            function emailValid(value) {
                return (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value);
            }

        },
        templateUrl: 'wp-content/themes/watergular/partials/contact.form.html'
    };
}]);
// Pop Image plugin ===================================================================================================
function addaptImages(elem) {
    var imgList = $(elem).find("img");
    for (var i = 0; i < imgList.length; i++) {
        if ($(imgList[i]).parent().is('figure')) {
            $(imgList[i]).parent().addClass('wp-image');
        } else {
            $(imgList[i]).wrap("<figure></figure>");
            $(imgList[i]).parent().addClass('wp-image');
        }
        if ($(imgList[i]).parent().parent().is('p')) $(imgList[i]).parent().unwrap();
        $(imgList[i]).parent().find('figcaption').addClass('wp-caption-text')
        $(imgList[i]).removeAttr('srcset')
    }
    for(var f = 0; f < imgList.length; f++) {
        if($(imgList[f]).parent().next().is('figure.wp-image') || $(imgList[f]).parent().prev().is('figure.wp-image')){
            $(imgList[f]).addClass('small-image');
        }
        if($(imgList[f]).parent().parent().is('a')) $(imgList[f]).parent().unwrap();
    }
    $(elem + ' img').on('click', function () {
        var el = $(this);
        var temp = this.src.split('/');
        temp[temp.length - 1] =getFullImageName(temp[temp.length - 1]);
        var link = temp.join('/');
        var wrap = $('<div class="pop-image">');
        var img = $('<img>');
        img.attr('src', link);
        img.attr('width', this.width);
        img.attr('height', this.height);
        wrap.append('<span class="close-pop-image">&times;</span>');
        wrap.append(img);
        $('body').append(wrap);

        img.on('load', function () {
            wrap.css({
                'top': getOffset(el).top + 'px',
                'left': getOffset(el).left + 'px',
                'width': el.width(),
                'height': el.height(),
                'opacity': 0.2
            });
            wrap.animate({
                'top': 0,
                'left': 0,
                'bottom': 0,
                'right': 0
            }, 200, function () {
                picResize(wrap[0]);
                img.css({
                    'max-width': img[0].naturalWidth,
                    'max-height': img[0].naturalHeight
                });

                wrap.animate({
                    'width': '100%',
                    'height': '100%',
                    'opacity': 1
                }, 300, function () {
                    $('body').addClass('fullscreen');
                    wrap.addClass('full-pop');
                    wrap.removeAttr('style');
                    picResize(wrap[0]);

                });

            });

        });
        $('.close-pop-image').on('click', function () {
            $('.pop-image').remove();
            $('body').removeClass('fullscreen');
        })
    });
    $('.gallery').on('click', function () {
        galleryIterate(this);
    });
    $('.wp-image').on('click', function (e) {
        e.preventDefault();
    });
    $(window).on('resize', function () {
        picResize($('.pop-image')[0])
    })
}

function picResize(wrap) {
    if (typeof wrap === 'object') {
        var img = $(wrap).find('img')[0];
        $(img).removeClass('high-image').removeClass('wide-image');

        var imgW = img.naturalWidth;
        var imgH = img.naturalHeight;
        var wrapW = wrap.clientWidth;
        var wrapH = wrap.clientHeight;

        // wide image corrections
        if (imgH < imgW && imgH >= wrapH && imgW < wrapW) $(img).addClass('high-image');
        else if (imgH < imgW && imgH < wrapH && imgW >= wrapW) $(img).addClass('wide-image');
        else if (imgH < imgW && imgH >= wrapH && imgW >= wrapW && wrapW <= wrapH) $(img).addClass('wide-image');
        else if (imgH < imgW && imgH >= wrapH && imgW >= wrapW && wrapW > wrapH) $(img).addClass('high-image');
        // extra width correction
        if (img.clientWidth > wrapW) $(img).removeClass('high-image').addClass('wide-image');

        // height image corrections
        if (imgH > imgW && imgH >= wrapH && imgW < wrapW) $(img).addClass('wide-image');
        else if (imgH > imgW && imgH < wrapH && imgW >= wrapW) $(img).addClass('high-image');
        else if (imgH > imgW && imgH >= wrapH && imgW >= wrapW && wrapW <= wrapH) $(img).addClass('high-image');
        else if (imgH > imgW && imgH >= wrapH && imgW >= wrapW && wrapW > wrapH) $(img).addClass('wide-image');
        // extra height correction
        if (img.clientHeight > wrapH) $(img).removeClass('wide-image').addClass('high-image');
    }
}

function getOffset(el) {
    el = el[0].getBoundingClientRect();
    return {
        left: el.left,
        top: el.top
    }
}


// Resizer ================================================================================

$(window).on('resize', function () {
    tableResize();
    iframeResize();
});


function tableResize() {
    var tb = $('table');
    for (var i = 0; i < tb.length; i++){
        if(!$(tb[i]).prev().is('div.responsive-table') && !$(tb[i]).parent().is('div.responsive-table')){
            generateSmallTable(tb[i])
        }
        if ($(tb[i]).width() > $('.re-frame').width() && $(tb[i]).prev().is('div.responsive-table')){
            $(tb[i]).prev().addClass('visible');
            $(tb[i]).hide();
        } else if ($(tb[i]).width() <= $('.re-frame').width() && $(tb[i]).prev().is('div.responsive-table')){
            $(tb[i]).prev().removeClass('visible');
            $(tb[i]).show();
        }
    }
}

function generateSmallTable(el) {
    $('<div class="responsive-table"></div>').insertBefore( el );
    var heads = $(el).find('th');
    var rows = $(el).find('tbody tr');
    for (var i = 0; i < rows.length; i++){
        var et = $(rows[i]).find('td');
        var rws = '<table><tbody>';
        for (var r = 0; r < et.length; r++){
            rws += '<tr>';
            if(r < heads.length && heads.length !== 0) rws += '<td class="first-column">' + $(heads[r]).html() + '</td>';
            rws += '<td class="second-column">' + $(et[r]).html() + '</td>';
            rws += '</tr>';
        }
        rws += '</tbody></table>';
        $('.responsive-table').append(rws);
    }
}

function iframeResize() {
    var frame = $('.re-frame').find('iframe');
    for (var i = 0; i < frame.length; i++){
        var w = $(frame[i]).attr('width');
        var h = $(frame[i]).attr('height');
        var win = $(frame[i]).parent().width();
        $(frame[i]).parent().css({'text-align': 'center'});
        if (win > 950) win = 950;
        $(frame[i]).css({'width': win+'px', 'height': (h*(win/w)) + 'px' })
    }
}

function panoramaViewer() {
    var imgList = $('.panorama360');

    for (var i = 0; i < imgList.length; i++) {
        var imgEl = $(imgList[i]).hasClass('wp-image') ? $(imgList[i]).find('img') : $(imgList[i]);
        var parent = $(imgList[i]).hasClass('wp-image') ? $(imgList[i]) : $(imgList[i]).parent();

        var img = getFullImageName(imgEl.attr('src'));
        imgEl.remove();
        parent.css('width', '100%');
        parent.prepend('<div class="loading-blob"></div>');
        parent.prepend('<div id="sphere-view-'+i+'" class="panorama-view" style="height: 500px; width:100%;"></div>');
        var itemIdx = ''+i;
        var sph = new PhotoSphereViewer({
            panorama: img,
            container: document.getElementById('sphere-view-' + itemIdx),
            time_anim: 3000,
            anim_speed: '1rpm',
            navbar: true,
            loading_msg: '',
            navbar_style: {
                backgroundColor: 'rgba(86, 177, 191, 0.7)',
                autorotateThickness: 2,
                zoomRangeThickness: 2
            },
            onready: function () {
                // parent.find('.loading-blob').hide();
            }
        });
    }
}

function getFullImageName(nm){
    try {
        var sufix = nm.match(/\.(jpg|png|gif|jpeg|bmp|JPG|PNG|GIF|JPEG|BMP)$/gm);
        return nm.replace(/-\d{2,4}x\d{2,4}\.(jpg|png|gif|jpeg|bmp|JPG|PNG|GIF|JPEG|BMP)$/gm, sufix[0]);
    } catch (e) {
        return nm;
    }
}
site.factory('getLang', ['$location', '$cookies', function ($location, $cookies) {
    return {
        param: function () {
            return $cookies.get('nkrp_langCookie') || 'lt';
        },
        set: function (lg) {
            var expireDate = new Date();
            expireDate.setFullYear(expireDate.getFullYear() + 1);
            $cookies.put('nkrp_langCookie', lg, {'expires': expireDate});
        }
    };
}]);

site.factory('linker', ['$location', function ($location) {
    return {
        slugify: function (str) {
            return str.trim().replace(/[^\w\s]/gi, '').replace(' ', '-').toLowerCase();
        },
        changeTitle: function (str) {
            var til = document.title.split('|');
            if (str && str.length > 0) document.title = str + ' | ' + til[til.length-1];
            else document.title = til[til.length-1];
        },
        getUriParam: function (key) {
            var results = new RegExp('[\?&]' + key + '=([^&#]*)').exec(window.location.href);
            try {
                return results[1];
            }
            catch (err) {
                return 'undefined';
            }
        },
        setUriParam: function (key, value) {
            key = encodeURI(key); value = encodeURI(value);
            $location.search(key, value);
        }
    };
}]);

site.factory('format', [function () {
    var form = {};
    form.dateFormat = function (date) {
        var dd = new Date(date);
        var dayOfWeek = translations.savaitesDienos[dd.getDay()];
        var month = translations.menesiai[dd.getMonth()];
        return dayOfWeek + ', ' + dd.getFullYear() + ' ' + month + ' ' + dd.getDate() + ' d. ' + ('0' + dd.getHours()).substr(-2) + ':' + ('0' +dd.getMinutes()).substr(-2);
    };
    form.eventDateFormat = function (date) {
        var dd = new Date(date);
        var dayOfWeek = translations.savaitesDienos[dd.getDay()];
        var month = translations.menesiai[dd.getMonth()];
        return dayOfWeek + ', ' + dd.getFullYear() + ' ' + month + ' ' + dd.getDate() + ' d.';
    };
    return form;
}]);

site.directive('resizeThis',  ['$window', function($window) {
    return {
        link: function(scope, elem, attrs) {
            scope.onResize = function() {
                var h = $(elem)[0].clientHeight;
                var w = $(elem)[0].clientWidth;
                var hi = $(elem).find('img')[0].clientHeight;
                var wi = $(elem).find('img')[0].clientWidth;
                // $(elem).removeClass('width-max').removeClass('height-max');
                $(elem).addClass('width-max');
                // if(w > h && wi >= hi) $(elem).addClass('width-max');
                // else if(w > h && wi < hi) $(elem).addClass('height-max');
                // else if(w < h && wi >= hi) $(elem).addClass('height-max');
                // else if(w < h && wi < hi) $(elem).addClass('width-max');
                if(wi< w)$(elem).addClass('width-max').removeClass('height-max');
                if(hi< h)$(elem).addClass('height-max').removeClass('width-max');
            };
            scope.onResize();

            angular.element($window).bind('resize', function() {
                scope.onResize();
            });

            elem.find('img').bind('load', function () {
                scope.onResize();
            });
        }
    }
}]);

site.directive('imageOnload',['$window', function($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.one('load', function() {
                scope.imgLoadedCallback(element)
            });
            element.on('activeresize', function() {
               scope.imgLoadedCallback(element);
            });
        }
    };
}]);
site.directive('mainFooter',  ['$window', 'wpjson', 'getLang', function($window, wpjson, getLang) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            scope.copy = new Date().getFullYear();
            scope.links = '';
            scope.contacts = '';
            scope.errorNF = '';
            scope.doneNF = '';
            wpjson.getPage('nuorodos').then(function (data) {
                scope.links = data;
            });
            wpjson.getPage( getLang.param() + '-kontaktai-apacia').then(function (data) {
                scope.contacts = data;
            });

        },
        templateUrl: 'wp-content/themes/watergular/partials/footer.html'
    };
}]);

site.directive('footerMenu',  ['$window', 'wpjson', 'getLang', function($window, wpjson, getLang) {
    var lang = getLang.param();
    return {
        restrict: 'A',
        scope: {
            menuTitle: '@'
        },
        link: function (scope, element, attributes) {
            scope.menuList = [];
            scope.lang = lang;
            wpjson.getMenus().then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].slug === lang + '-' + attributes.menuTitle) {
                        wpjson.getMenu(data[i].ID).then(function (item) {
                            scope.menuList = item.items;
                        }, function () {
                        });
                        break;
                    }
                }
            }, function () {});
        },
        templateUrl: 'wp-content/themes/watergular/partials/footer.menu.html'
    };
}]);


site.directive('mainHeaderImg',  ['$window', function($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            scope.hh  = $window.innerHeight;
            scope.title = translations.title;
            // background
            var val = -50;
            scope.endless = val;
            angular.element($window).bind("scroll", function() {
                var offset = this.pageYOffset/2; // Regulates speed
                if(document.querySelector('.main-header-img') !== null && offset > 0){
                    //background
                    var cal = (80 * offset-$window.innerHeight)/$window.innerHeight;
                    scope.endless = this.pageYOffset === 0 ? val : val + cal;
                    scope.$apply();
                }
            });
            angular.element($window).bind('resize', function () {
                scope.hh  = $window.innerHeight;
                scope.$apply();
            })
        },
        templateUrl: 'wp-content/themes/watergular/partials/header.main.html'
    };
}]);
site.directive('miniHeader',  ['$window', function($window) {
    return {
        restrict: 'A',
        scope: {
            headerImg: '@'
        },
        link: function (scope, element, attributes) {
            attributes.$observe('headerImg', function (src) {
                scope.imgSrc = src;
            });

            var val = 0; // -30
            scope.hh  = $window.innerHeight;
            scope.endless = val;
            angular.element($window).bind("scroll", function(event) {
                var offset = this.pageYOffset;
                if(document.querySelector('.small-header-img') !== null && offset > 0){
                    var cal = (80 * offset-$window.innerHeight)/$window.innerHeight;
                    scope.endless = cal <= val ? val : val + cal;
                    scope.$apply();
                }
            });
            angular.element($window).bind("resize", function() {
                var head = $('.small-header-img');
                var w = head.find('img').width();
                var wf = head.width();
                var h = head.find('img').height();
                var hf = head.height();
            })

        },
        templateUrl: 'wp-content/themes/watergular/partials/header.small.html'
    };
}]);
site.directive('navigationMenu', ['getLang', 'wpjson', '$document', function (getLang, wpjson, $document ) {
    var lang = getLang.param();
    return {
        restrict: 'A',
        scope: {
            navigationMenu: '@'
        },
        link: function (scope, element, attributes) {
            scope.menuList = [];
            scope.lang = lang;
            scope.homeUrl = location.origin + '/';
            wpjson.getMenus().then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].slug === lang + '-' + attributes.navigationMenu) {
                        wpjson.getMenu(data[i].ID).then(function (item) {
                            item.items.push({
                                title: translations.renginiai,
                                object: 'e',
                                object_slug: 'e/' +translations.renginiai.toLowerCase()+'?pg=1',
                                url: '/e/' +translations.renginiai.toLowerCase()+'?pg=1'
                            });
                            item.items.push({
                               title: translations.naujienos,
                                object: 'n',
                                object_slug: 'n/' + translations.naujienos.toLowerCase()+'?pg=1',
                                url: '/n/' + translations.naujienos.toLowerCase()+'?pg=1'
                            });
                            item.items.push({
                                title: translations.kontaktai,
                                object: 'k',
                                object_slug: 'k/' +translations.kontaktai.toLowerCase(),
                                url: '/k/' +translations.kontaktai.toLowerCase()
                            });
                            scope.menuList = item.items;
                        }, function () {
                        });
                        break;
                    }
                }
            }, function () {
            });
        },
        templateUrl: 'wp-content/themes/watergular/partials/menu.toolbar.html'
    };
}]);
site.directive('weatherBreeze', ['getLang', 'wpjson', '$document', function (getLang, wpjson, $document) {
    var lang = getLang.param();
    return {
        restrict: 'A',
        scope: {
            navigationMenu: '@'
        },
        link: function (scope, element, attributes) {
            wpjson.getWeather().then(function (res) {
                if (res !== null && typeof res.sys !== 'undefined') {
                    var now = res.now * 1000;
                    var dN = 'day';
                    var sunrise = res.sys.sunrise * 1000;
                    var sunset = res.sys.sunset * 1000;

                    if (new Date(now) > new Date(sunrise) && new Date(now) < new Date(sunset)) dN = 'day';
                    else dN = 'night';
                    scope.weatherIcon = getWeatherIcon(res.weather[0].id, dN);
                    scope.temp = res.main.temp;
                    scope.city = res.name;
                    scope.weekday = translations.savaitesDienos[new Date(res.dt * 1000).getDay()];
                } else {
                    scope.temp = 'N/A';
                }
            }, function () {
                scope.temp = 'N/A';
                console.log('Unsuccessful API request!');
            });
            wpjson.getWeatherWeek().then(function (res) {
                var temp = [];
                if(typeof res.list !== 'undefined') {
                    var wList = res.list;
                    for (var i = 0; i < wList.length; i++) {
                        temp.push({
                            day: translations.savaitesDienosTrump[new Date(wList[i].dt * 1000).getDay()],
                            tempDay: wList[i].temp.day,
                            tempNight: wList[i].temp.night,
                            wind: wList[i].speed,
                            humidity: wList[i].humidity,
                            icon: getWeatherIcon(wList[i].weather[0].id, 'day')
                        });
                    }
                }
                scope.items = temp;
            },function () {
                console.log('Unsuccessful API request!');
            });
            scope.show = false;

            element.on('click', function (e) {
                e.stopPropagation();
            });

            element.on('click', function () {
                scope.show = !scope.show;
                if (scope.show) element.addClass('active');
                else element.removeClass('active');
                scope.$apply();
            });

            element.find('.weather-week-wrap').on('click', function (e) {
                e.stopPropagation();
            });

            $document.find('.lang-active').on('click', function () {
                scope.show = false;
                element.removeClass('active');
                scope.$apply();
            });

            $document.on('click', function () {
                scope.show = false;
                element.removeClass('active');
                scope.$apply();
            });
        },
        templateUrl: 'wp-content/themes/watergular/partials/weather.html'
    };

    function getWeatherIcon(wId, dayTime) {
        if (wId >= 200 && wId < 300) { // Thunderstorm
            return 'thunder';
        } else if (wId >= 300 && wId < 400) { //Drizzle
            return 'drizzle';
        } else if (wId >= 500 && wId < 600) { //Rain
            return 'rain';
        } else if (wId >= 600 && wId < 700) { //Snow
            return 'snow';
        } else if (wId >= 700 && wId < 800 ) { //Fog
            return 'fog';
        } else if (wId === 800 || wId === 951) { //Clear
            return 'clear-' + dayTime;
        } else if (wId === 801 || wId === 802) { // Bit cloudy
            return 'cloudy-' + dayTime;
        } else if (wId > 802 && wId < 900) { //Cloudy
            return 'cloudy';
        } else if (wId === 903) { //Cold
            return 'cold';
        } else if (wId === 904) { //Hot
            return 'hot';
        } else if (wId > 900) { //Windy
            return 'windy';
        }
        return '';
    }
}]);
site.factory('wpjson', ['$http', '$q', function ($http, $q) {
    var wpjson = {};
    var API_URL = location.origin + '/wp-json/';
    //************** Pages ***********************
    wpjson.getPage = function (slug) {
        var deferred = $q.defer();
        $http.get(location.origin + '/wp-json/wp/v2/pages/?slug=' + slug).then(function (res) {
            deferred.resolve(res.data[0]);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Posts ***********************
    wpjson.getPosts = function (cat, count) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/categories/?slug=' + cat).then(function (res) {
            $http.get(API_URL + 'wp/v2/posts/?categories=' + res.data[0].id + '&per_page=' + count).then(function (res) {
                deferred.resolve(res.data);
            }, function (err) {
                deferred.reject(err);
            });
        });
        return deferred.promise;
    };

    wpjson.getPost = function (slug) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/posts/?slug=' + slug).then(function (res) {
            deferred.resolve(res.data[0]);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getPostsInPage = function (cat, page) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/categories/?slug=' + cat).then(function (res) {
            $http.get(API_URL + 'wp/v2/posts/?categories=' + res.data[0].id + '&per_page=10&page=' + page).then(function (res) {
                deferred.resolve({posts: res.data, totalPages: parseInt(res.headers('X-WP-TotalPages'))});
            }, function (err) {
                deferred.reject(err);
            });
        });
        return deferred.promise;
    };

    wpjson.searchQuery = function (query, page) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/posts/?search=' + query).then(function (res) { //+ '&per_page=10&page=' + page
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Menus ***********************
    wpjson.getMenus = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp-api-menus/v2/menus').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getMenu = function (id) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp-api-menus/v2/menus/' + id).then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Featured Image ***********************
    wpjson.getFeaturedImage = function (obj, type) {
        var deferred = $q.defer();
        if (typeof obj !== 'undefined') {
            var id = obj.featured_media;
            var idx = obj.id;
            if (typeof obj.featured_media !== 'undefined' && obj.featured_media !== 0) {
                $http.get(API_URL + 'wp/v2/media/' + id).then(function (res) {
                    if (res.data.length > 0) deferred.resolve(undefined);
                    else if (type === 'thumb' && typeof res.data.media_details.sizes.thumbnail !== 'undefined') deferred.resolve({
                        url: res.data.media_details.sizes.thumbnail.source_url,
                        id: idx,
                        width: res.data.media_details.sizes.thumbnail.width,
                        height: res.data.media_details.sizes.thumbnail.height
                    });
                    else if (type === 'medium' && typeof res.data.media_details.sizes.medium !== 'undefined') deferred.resolve({
                        url: res.data.media_details.sizes.medium.source_url,
                        id: idx,
                        width: res.data.media_details.sizes.medium.width,
                        height: res.data.media_details.sizes.medium.height

                    });
                    else deferred.resolve({
                            url: res.data.source_url,
                            id: idx,
                            width: res.data.media_details.width,
                            height: res.data.media_details.height
                        });
                }, function (err) {
                    deferred.reject(err);
                });
            } else {
                deferred.reject();
            }
        } else {
            deferred.reject();
        }
        return deferred.promise;
    };
    //************** Weather ***********************
    wpjson.getWeather = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'breeze/v1/today').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getWeatherWeek = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'breeze/v1/week').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Gallery ***********************

    wpjson.getAlbums = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'slash/v2/albums').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getGallery = function (id) {
        var deferred = $q.defer();
        $http.get(API_URL + 'slash/v2/album/'+id).then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    //************** Email form ***********************
    wpjson.sendEmail = function (obj) {
        var deferred = $q.defer();
        $http.post(API_URL + 'email/v1/send', obj).then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };


    return wpjson;
}]);