site.controller('contactsCtrl', ["$scope", "wpjson", "$routeParams", 'getLang', 'linker', '$rootScope', function ($scope, wpjson, $routeParams, getLang, linker, $rootScope) {
    linker.changeTitle(translations.kontaktai);
    $rootScope.sideBar = false;

    wpjson.getPage(getLang.param() + '-kontaktai').then(function (data) {
        $scope.page = data;
        $scope.$watch('page', function () {
            addaptImages('.page-text-render');
            tableResize();
        });
        wpjson.getFeaturedImage(data).then(function (img) {
            $scope.featuredImage = img.url;
        }, function () {
        })
    });

    var overlay = $('.map-contact .overlay');
    overlay.show();
    overlay.on('click', function () {
        overlay.hide();
    });

    $('.map-contact iframe').on('mouseleave', function () {
        overlay.show();
    });
}]);
site.controller('eventsCtrl', ["$scope", "wpjson", "$routeParams", "linker", 'getLang', 'format', '$rootScope', function ($scope, wpjson, $routeParams, linker, getLang, format, $rootScope) {
    linker.changeTitle(translations.renginiai);
    $scope.featuredImage = '';
    $scope.events = [];
    $scope.pages = [];
    $rootScope.sideBar = false;
    var currentPage = linker.getUriParam('pg');
    $scope.currentPage = currentPage !== 'undefined' ? parseInt(currentPage) : 1;
    jumpToThisPage($scope.currentPage);
    $scope.jumpToPage = function (itm) {
        switch (itm) {
            case 'first':
                jumpToThisPage(1);
                break;
            case 'prev':
                jumpToThisPage($scope.currentPage - 1);
                break;
            case 'next':
                jumpToThisPage($scope.currentPage + 1);
                break;
            case 'last':
                jumpToThisPage($scope.pages.length);
                break;
            default:
                if (itm !== $scope.currentPage) jumpToThisPage(itm);
                break;
        }
    };

    function jumpToThisPage(pg) {
        $scope.events = [];
        $scope.pages = [];
        var currentLang = 'lt'; //getLang.param()
        wpjson.getPostsInPage(currentLang + '-renginiai', pg).then(function (data) {
            $scope.events = reorderEvents('date_select', data.posts);
            $scope.currentPage = pg;
            for (var p = 0; p < data.totalPages; p++) $scope.pages.push(p + 1);

            for (var i = 0; i < data.posts.length; i++) {
                $scope.events[i].more = location.origin + '/' + $scope.events[i].slug;
                $scope.events[i].date = format.eventDateFormat($scope.events[i].date_select);
                wpjson.getFeaturedImage($scope.events[i], 'thumb').then(function (img) {
                    for (var j = $scope.events.length - 1; j >= 0; j--) {
                        if (j === 0) $scope.featuredImage = img.url.replace('-' + img.width + 'x' + img.height, '');
                        else if ($scope.featuredImage.length === 0) $scope.featuredImage = img.url.replace('-' + img.width + 'x' + img.height, '');

                        if (img && $scope.events[j].id === img.id) {
                            $scope.events[j].eventImage = img.url;
                            break
                        }
                    }
                }, function () {
                })
            }
            linker.setUriParam('pg', pg)
        })
    }

    function reorderEvents(key, data) {
        var temp = [];
        var today = new Date().setHours(0, 0, 0, 0);

        for (var i = 0; i < data.length; i++) {
            var dd = new Date(data[i][key]).getTime();
            if (today === dd) data[i].period = 'today';
            else if (today < dd) data[i].period = 'future';
            else data[i].period = 'past';
            for (var j = 0; j < temp.length; j++) {
                var dt = new Date(temp[j][key]).getTime();
                if (dd <= dt) {
                    temp.splice(j, 0, data[i]);
                    break
                } else if (j === temp.length - 1) {
                    temp.push(data[i]);
                    break
                }
            }
            if (temp.length === 0) temp.push(data[i]);
        }
        return temp.reverse();
    }
}]);
site.controller('galleryCtrl', ['$scope', 'wpjson', 'getLang', 'linker', '$window', '$document', '$rootScope',
    function ($scope, wpjson, getLang, linker, $window, $document, $rootScope) {
        linker.changeTitle(translations.galerija);
        $scope.featuredImage = './wp-content/themes/watergular/img/boxes/galllery-min.JPG';
        $scope.albums = [];
        $scope.gallery = [];
        $scope.selected = 0;
        $rootScope.sideBar = false;
        var lang = getLang.param();
        wpjson.getAlbums().then(function (alb) {
            for (var i = 0; i < alb.length; i++) {
                var nameAr = alb[i].album_name_feed.filter(function (item) {
                    return item.lang === lang;
                });

                var name = (nameAr.length > 0 ) ? nameAr[0].name : alb[i].slug;
                $scope.albums.push({
                    id: alb[i].id,
                    name: name
                })
            }
            if (alb.length > 0) getSelectedAlbum(0, alb[0].id);
        });
        $scope.changeGallery = getSelectedAlbum;
        var galWrap = $('.gallery-render');
        var wrapWidth = galWrap.width();
        var colCount = Math.floor(wrapWidth / 250);
        var elmWidth = wrapWidth / colCount;
        var trackColumns = [];
        for (var i = 0; i < colCount; i++)trackColumns.push(0);

        function getSelectedAlbum(idx, id) {
            $scope.selected = idx;
            galWrap.height(200);
            trackColumns = [];  // reset variables
            for (var i = 0; i < colCount; i++)trackColumns.push(0);
            $scope.gallery = [];
            $scope.loadShow = true;
            wpjson.getGallery(id).then(function (gal) {
                for (var i = 0; i < gal.length; i++) {
                    var name = '';
                    if (gal[i].img_name_feed !== null) {
                        if (typeof gal[i].img_name_feed[lang] !== 'undefined') name = gal[i].img_name_feed[lang];
                    }

                    $scope.gallery.push({
                        img: gal[i].thumb,
                        name: name
                    })
                }
                $scope.loadShow = false;
            }, function () {
                $scope.loadShow = false;
            });
        }

        var resizeId;
        $($window).on('resize', function () {
            wrapWidth = galWrap.width();
            colCount = Math.floor(wrapWidth / 250);
            elmWidth = wrapWidth / colCount;
            trackColumns = [];
            for (var i = 0; i < colCount; i++) trackColumns.push(0);
            clearTimeout(resizeId);
            resizeId = setTimeout(function () {
                $('.gallery-item-thumb').trigger('activeresize');
            }, 200);
        });

        $scope.imgLoadedCallback = function (elm) {
            var margin = 6;
            var el = $(elm);
            var par = el.parent();
            var shortColumn = trackColumns.indexOf(Math.min.apply(Math, trackColumns));
            var topPos = trackColumns[shortColumn];

            var offset = (margin / 3) * 2;
            var h = (el[0].naturalHeight / el[0].naturalWidth) * (elmWidth - offset);

            el.width(elmWidth - offset).height(h);
            par.width(elmWidth - offset).height(h);
            trackColumns[shortColumn] += h + margin;

            var left = shortColumn * (elmWidth - offset);
            if (shortColumn !== 0) left += (margin * shortColumn);

            par.css({'left': left + 'px', 'top': topPos + 'px'});
            par.animate({'opacity': 1});
            galWrap.height(Math.max.apply(Math, trackColumns));
        };

        $($document).on('click', '.gallery-item-caption', function () {
            var el = $(this).parent().find('img').first();
            var link = el[0].src.replace('thumb/', '');
            var wrap = $('<div class="pop-image">');
            var img = $('<img>');
            img.attr('src', link);
            img.attr('width', this.width);
            img.attr('height', this.height);
            img.attr('data-idx', $(this).parent().index());

            var prevImg = $(this).parent().prev().first().find('img');
            var nextImg = $(this).parent().next().first().find('img');
            if (nextImg.length > 0)
                wrap.append('<span class="next-pop-image" data-imgsrc="' + nextImg[0].src.replace('/thumb', '') + '" data-idx="' + $(this).parent().next().first().index() + '"></span>');
            else
                wrap.append('<span class="next-pop-image" style="display: none"></span>');

            if (prevImg.length > 0)
                wrap.append('<span class="prev-pop-image" data-imgsrc="' + prevImg[0].src.replace('/thumb', '') + '" data-idx="' + $(this).parent().prev().first().index() + '"></span>');
            else
                wrap.append('<span class="prev-pop-image" style="display: none"></span>');

            wrap.append('<span class="close-pop-image">&times;</span>');
            wrap.append(img);
            $('body').append(wrap);
            img.on('load', function () {
                wrap.css({
                    'top': getOffset(el).top + 'px',
                    'left': getOffset(el).left + 'px',
                    'width': el.width(),
                    'height': el.height(),
                    'opacity': 0.2
                });
                wrap.animate({
                    'top': 0,
                    'left': 0,
                    'bottom': 0,
                    'right': 0
                }, 200, function () {
                    picResize(wrap[0]);
                    img.css({
                        'max-width': img[0].naturalWidth,
                        'max-height': img[0].naturalHeight
                    });

                    wrap.animate({
                        'width': '100%',
                        'height': '100%',
                        'opacity': 1
                    }, 300, function () {
                        $('body').addClass('fullscreen');
                        wrap.addClass('full-pop');
                        wrap.removeAttr('style');
                        picResize(wrap[0]);
                        img.off('load');
                    });

                });
            });
            $('.close-pop-image').on('click', function () {
                $('.pop-image').remove();
                $('body').removeClass('fullscreen');
            });
            $('.prev-pop-image').on('click', function () {
                var getSrc = $(this).data('imgsrc');
                var ind = $(this).data('idx');
                var nextBtn = $('.next-pop-image');
                //set next button
                nextBtn.show();
                var curr = $('.pop-image').find('img');
                var nextItm = curr[0].src;
                nextBtn.data('imgsrc', nextItm).data('idx', curr.data('idx'));
                //set main img
                curr.data('idx', ind);
                curr[0].src = getSrc;
                //set prev button
                var prevItem = $($('.gallery-render').find('.gallery-item').get(parseInt(ind))).prev('.gallery-item');
                if (prevItem.length > 0) {
                    $(this).data('idx', prevItem.index());
                    $(this).data('imgsrc', prevItem.find('img')[0].src.replace('/thumb', ''));
                } else {
                    $(this).hide();
                }
                curr.on('load', function () {
                    $(this).removeAttr('style');
                    picResize($('.pop-image')[0]);
                    $(this).off('load')
                });
            });
            $('.next-pop-image').on('click', function () {
                var getSrc = $(this).data('imgsrc');
                var ind = $(this).data('idx');
                var prevBtn = $('.prev-pop-image');
                //set prev button
                prevBtn.show();
                var curr = $('.pop-image').find('img');
                var prevItm = curr[0].src;
                prevBtn.data('imgsrc', prevItm).data('idx', curr.data('idx'));
                //set main img
                curr.data('idx', ind);
                curr[0].src = getSrc;
                //set next button
                var nextItem = $($('.gallery-render').find('.gallery-item').get(parseInt(ind))).next('.gallery-item');
                if (nextItem.length > 0) {
                    $(this).data('idx', nextItem.index());
                    $(this).data('imgsrc', nextItem.find('img')[0].src.replace('/thumb', ''));
                } else {
                    $(this).hide();
                }
                curr.on('load', function () {
                    $(this).removeAttr('style');
                    picResize($('.pop-image')[0]);
                    $(this).off('load')
                });

            })
        });

        $(window).on('resize', function () {
            picResize($('.pop-image')[0])
        })

    }]);
site.controller('mainCtrl', ['$scope', 'wpjson', 'getLang', 'linker', 'format', '$window', '$sce', '$rootScope',
    function ($scope, wpjson, getLang, linker, format, $window, $sce, $rootScope) {
        $rootScope.sideBar = false;
        var currentLang = 'lt'; //getLang.param()

        linker.changeTitle('');
        //events section =====================================
        $scope.events = [];
        wpjson.getPosts( currentLang + '-renginiai', 3).then(function (data) {
            $scope.events = reorderEvents('date_select', data);
            for (var i = 0; i < $scope.events.length; i++) {
                $scope.events[i].more = location.origin + '/' + $scope.events[i].slug;
                $scope.events[i].date = format.eventDateFormat($scope.events[i].date_select);
                wpjson.getFeaturedImage($scope.events[i], 'thumb').then(function (img) {
                    for (var j = $scope.events.length - 1; j >= 0; j--) {
                        if (img && $scope.events[j].id === img.id) {
                            $scope.events[j].eventImage = img.url;
                            break;
                        }
                    }
                }, function () {
                })
            }
        }, function () {
        });

        function reorderEvents(key, data) {
            var temp = [];
            var today = new Date().setHours(0, 0, 0, 0);

            for (var i = 0; i < data.length; i++) {
                var dd = new Date(data[i][key]).getTime();
                if (today === dd) data[i].period = 'today';
                else if (today < dd) data[i].period = 'future';
                else data[i].period = 'past';
                for (var j = 0; j < temp.length; j++) {
                    var dt = new Date(temp[j][key]).getTime();
                    if (dd <= dt) {
                        temp.splice(j, 0, data[i]);
                        break;
                    } else if (j === temp.length - 1) {
                        temp.push(data[i]);
                        break;
                    }
                }
                if (temp.length === 0) {
                    temp.push(data[i]);
                }
            }
            return temp.reverse();
        }

        // news section ======================================
        $scope.newsLink = '/n/' + translations.naujienos.toLowerCase() + '?pg=1';
        $scope.posts = null;
        wpjson.getPosts(currentLang + '-naujienos', 3).then(function (data) {
            $scope.posts = data;
            for (var i = 0; i < data.length; i++) {
                $scope.posts[i].more = location.origin + '/' + data[i].slug;
                wpjson.getFeaturedImage(data[i], 'medium').then(function (img) {
                    for (var j = $scope.posts.length - 1; j >= 0; j--) {
                        if (img && $scope.posts[j].id === img.id) {
                            $scope.posts[j].newsImage = img.url;
                            $scope.posts[j].newsImageSize = {width: img.width, height: img.height};
                            break
                        }
                    }
                }, function () {
                })
            }

        }, function () {
        });
        $scope.resp = false;
        newsSizes();
        $(window).on('resize', newsSizes);
        function newsSizes() {
            $scope.resp = $(window).width() < 900;
        }

        // boxes ==============================================
        boxSizes();
        $(window).on('resize', boxSizes);
        $scope.gallerylink = '/g/' + linker.slugify(translations.galerija);
        $scope.naturelink = '/s/' + linker.slugify(translations.gamtosMokykla);
        $scope.placeslink = '/v/' + linker.slugify(translations.lankytinosVietos);
        $scope.productBrand = '/p/produkto-zenklas';
        $scope.shownatureschool = getLang.param() === 'lt';
        $scope.showproductbrand = getLang.param() === 'lt';
        function boxSizes() {
            var ww = $(window).width();
            var wd = (ww * 0.7); // 0.7 -> 70%
            if (ww > 1200) wd = 1200 * 0.7;
            var wh = wd / 3;
            if (ww < 600) $('.pop-box').width('100%').height($('.pop-box').width());
            else $('.pop-box').width(wh).height(wh);
            $('.boxes-wrapper').css('top', ((wh / 3) * -1) + 'px');

        }
        // write to us =========================================

    }]);


site.controller('newsCtrl', ["$scope", "wpjson", "$routeParams", "linker", 'getLang', 'format', "$rootScope", function ($scope, wpjson, $routeParams, linker, getLang, format,$rootScope) {
    linker.changeTitle(translations.naujienos);
    $scope.featuredImage = '';
    $scope.posts = [];
    $scope.pages = [];
    $rootScope.sideBar = false;
    var currentPage = linker.getUriParam('pg');
    $scope.currentPage = currentPage !== 'undefined' ? parseInt(currentPage) : 1;
    jumpToThisPage($scope.currentPage);
    $scope.jumpToPage = function (itm) {
        switch (itm) {
            case 'first':
                jumpToThisPage(1);
                break;
            case 'prev':
                jumpToThisPage($scope.currentPage - 1);
                break;
            case 'next':
                jumpToThisPage($scope.currentPage + 1);
                break;
            case 'last':
                jumpToThisPage($scope.pages.length);
                break;
            default:
                if (itm !== $scope.currentPage) jumpToThisPage(itm);
                break;
        }
    };
    function jumpToThisPage(pg) {
        $scope.posts = [];
        $scope.pages = [];
        var currentLang = 'lt'; //getLang.param()
        wpjson.getPostsInPage(currentLang + '-naujienos', pg).then(function (data) {
            $scope.posts = data.posts;
            $scope.currentPage = pg;

            for (var p = 0; p < data.totalPages; p++) $scope.pages.push(p + 1);

            for (var i = 0; i < data.posts.length; i++) {
                $scope.posts[i].more = location.origin + '/' + data.posts[i].slug;
                $scope.posts[i].date = format.dateFormat(data.posts[i].date);
                wpjson.getFeaturedImage(data.posts[i], 'thumb').then(function (img) {
                    for (var j = data.posts.length - 1; j >= 0; j--) {
                        if(j === 0) $scope.featuredImage = img.url.replace('-'+img.width+'x'+img.height, '');
                        else if ($scope.featuredImage.length === 0) $scope.featuredImage = img.url.replace('-'+img.width+'x'+img.height, '');
                        if (img && data.posts[j].id === img.id) {
                            $scope.posts[j].newsImage = img.url;
                            break;
                        }
                    }
                })
            }
            linker.setUriParam('pg', pg)
        })
    }
}]);
site.controller('pageCtrl', ["$scope", "wpjson", "$routeParams", "linker", "$sce", '$rootScope', function ($scope, wpjson, $routeParams, linker, $sce, $rootScope) {
    $rootScope.sideBar = false;
    wpjson.getPage($routeParams.slug).then(function (data) {
        $scope.page = data;
        $scope.trusted = $sce.trustAsHtml(data.content.rendered.replace(/<a\shref=/g, '<a target="_blank" href='));
        linker.changeTitle(data.title.rendered);
        $scope.$watch('page', function() {
            addaptImages('.page-text-render');
            tableResize();
            iframeResize();
            panoramaViewer();
        });
        wpjson.getFeaturedImage(data).then(function (img) {
            $scope.featuredImage = img.url;
        })
    }, function () {
    });
}]);
site.controller('placesCtrl', ['$scope', 'wpjson', 'getLang', 'linker', '$rootScope', function ($scope, wpjson, getLang, linker, $rootScope) {
    linker.changeTitle(translations.lankytinosVietos );
    $scope.featuredImage = './wp-content/themes/watergular/img/boxes/places-min.jpg';
    $rootScope.sideBar = false;
    var overlay = $('.map-contact .overlay');
    overlay.show();
    overlay.on('click', function () {
        overlay.hide();
    });

    $('.map-contact iframe').on('mouseleave', function () {
        overlay.show();
    });
}]);
site.controller('postCtrl', ["$scope", "wpjson", "$routeParams", "format", "linker", "$sce", '$rootScope', function ($scope, wpjson, $routeParams, format, linker, $sce, $rootScope) {
    $rootScope.sideBar = false;
    wpjson.getPost($routeParams.slug).then(function (data) {
        data.date = format.dateFormat(data.date);

        $scope.post = data;
        $scope.trusted = $sce.trustAsHtml(data.content.rendered.replace(/<a\shref=/g, '<a target="_blank" href='));
        linker.changeTitle(data.title.rendered);
        $scope.fbShare = '//www.facebook.com/sharer/sharer.php?u=' + location.href;
        $scope.twShare = '//twitter.com/home?status=' + location.href.replace(':', '%3A');
        $scope.gpShare = '//plus.google.com/share?url=' + 'google.lt';
        $scope.$watch('post', function () {
            addaptImages('.post-text-render');
            tableResize();
            iframeResize();
            panoramaViewer();
        });
        wpjson.getFeaturedImage(data).then(function (img) {
            $scope.featuredImage = img.url;
        })
    });
}]);
site.controller('rootCtrl', ['$scope', '$rootScope', '$http', 'getLang', '$location', '$window', '$document', '$cookies',
        function ($scope, $rootScope, $http, getLang, $location, $window, $document, $cookies ) {
            $rootScope.sideBar = false;
            $scope.searchActive = false;
            $scope.searchInput = '';
            $scope.langList = langs;
            $scope.activeLang = getLang.param();
            $scope.openList = false;

            $('.lang-active').on('click', function (e) {
                $scope.openList = !$scope.openList;
                $scope.searchActive = false;
                $scope.$apply();
                e.stopPropagation();
            });
            $document.find('.weather-wrap').on('click', function () {
                $scope.openList = false;
                $scope.searchActive = false;
            });

            $scope.changeLang = function (lg) {
                getLang.set(lg);
                $window.location.href = $window.location.protocol + '//' + $window.location.host + '/';
            };

            $scope.sideBarBtn = function () {
              $rootScope.sideBar = !$rootScope.sideBar;
            };
            // search ================================

            $scope.searchBtn = function (e) {
                $scope.openList = false;
                $scope.searchActive = !$scope.searchActive;
                // e.stopPropagation();
            };

            $('.search-input-field').on('keyup', function (e) {
                if (e.keyCode === 13) {
                    var query = $scope.searchInput.trim();
                    query = query.match(/[\d|a-zA-Ząčęėįšųūž]+/gi);

                    if(query){
                        query = query.join('+');
                        $window.location.href = $window.location.protocol + '//' + $window.location.host + '/q?query=' + encodeURIComponent(query);
                    }
                }
            });



            $('.search-input, .search-input input').on('click', function (e) {
               e.stopPropagation();
            });
            // document click reset ==================
            $document.on('click', function () {
                $scope.openList = false;
            });
            // up up and go ==========================
            $($window).on('scroll', function () {
               $scope.showScrollBtn = $window.pageYOffset > $window.innerHeight;
            });

            $scope.scrollToTop = function () {
                TweenLite.to($window, 1, {scrollTo:0});
            };

            // cookies notice ========================

            $scope.showCookiesNotice = !$cookies.get('nkrp_cookiesNotice');
            $scope.cookieMsgClose =  function () {
                var expireDate = new Date();
                expireDate.setFullYear(expireDate.getFullYear() + 1);
                $cookies.put('nkrp_cookiesNotice', true, {'expires': expireDate});
                $scope.showCookiesNotice = false;
            };
        }]);
site.controller('schoolCtrl', ['$scope', 'wpjson', 'getLang', 'linker', '$rootScope', function ($scope, wpjson, getLang, linker, $rootScope) {
    $scope.title = translations.gamtosMokykla || 'Gamtos mokykla';
    linker.changeTitle($scope.title);
    $scope.featuredImage = './wp-content/themes/watergular/img/boxes/school-min.JPG';
    $rootScope.sideBar = false;
    $scope.tabs = {};
    $scope.tabContents = [];
    $scope.wid = 0;
    $scope.setted = 0;
    wpjson.getMenus().then(function (data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].slug === getLang.param() + '-gamtos-mokykla') {
                wpjson.getMenu(data[i].ID).then(function (item) {
                    $scope.tabs = item.items;
                    $scope.wid = 100 / item.items.length;

                    for (var j = 0; j < item.items.length; j++) {
                        // $scope.tabContents[j] = '';
                        wpjson.getPage(item.items[j].object_slug).then((function (pg) {
                            $scope.tabContents.push(pg);
                        }))
                    }
                }, function () {
                });
                break;
            }
        }
    }, function () {
    });

    $scope.newTab = function (id, idx) {
        $('.tab-content').hide();
        $scope.setted = idx;
        $('.tab-content.content-' + id).fadeIn('slow');
        $('.tabs-line .selector').animate({'left': idx * $scope.wid + '%'}, 'fast')
    }
}]);
site.controller('searchCtrl', ['$scope', 'wpjson', "$routeParams", 'getLang', 'linker', '$rootScope', 'format', function ($scope, wpjson, $routeParams, getLang, linker, $rootScope, format) {
    var queryParam = location.search || '';
    if (queryParam.length > 0) queryParam = decodeURIComponent(queryParam).split("?")[1].split("&")[0].replace('query=', '');

    linker.changeTitle(translations.paieska);
    $scope.featuredImage = './wp-content/themes/watergular/img/headers/search.jpg';
    $scope.posts = [];
    $scope.pages = [];
    $scope.query = queryParam.replace('+', ' ');
    $rootScope.sideBar = false;
    var currentPage = linker.getUriParam('pg');
    $scope.currentPage = currentPage !== 'undefined' ? parseInt(currentPage) : 1;

    if (queryParam.trim().length > 0) {
        jumpToThisPage($scope.currentPage);
        $scope.jumpToPage = function (itm) {
            switch (itm) {
                case 'first':
                    jumpToThisPage(1);
                    break;
                case 'prev':
                    jumpToThisPage($scope.currentPage - 1);
                    break;
                case 'next':
                    jumpToThisPage($scope.currentPage + 1);
                    break;
                case 'last':
                    jumpToThisPage($scope.pages.length);
                    break;
                default:
                    if (itm !== $scope.currentPage) jumpToThisPage(itm);
                    break;
            }
        };

        function jumpToThisPage(pg) {
            $scope.posts = [];
            $scope.pages = [];
            wpjson.searchQuery(queryParam, pg).then(function (data) {
                $scope.posts = data;
                $scope.currentPage = pg;
                $scope.finishSearch = true;
                for (var p = 0; p < data.totalPages; p++) $scope.pages.push(p + 1);
                data.forEach(function (itm, idx) {
                    if (itm.date_select) $scope.posts[idx].date = format.eventDateFormat(itm.date_select);
                    else $scope.posts[idx].date = format.dateFormat(itm.date);
                    $scope.posts[idx].more = location.origin + '/' + itm.slug;
                });

                linker.setUriParam('pg', pg)
            })
        }
    } else {
        $scope.finishSearch = true;
    }

}]);