site.directive('mainHeaderImg',  ['$window', function($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            scope.hh  = $window.innerHeight;
            scope.title = translations.title;
            // background
            var val = -50;
            scope.endless = val;
            angular.element($window).bind("scroll", function() {
                var offset = this.pageYOffset/2; // Regulates speed
                if(document.querySelector('.main-header-img') !== null && offset > 0){
                    //background
                    var cal = (80 * offset-$window.innerHeight)/$window.innerHeight;
                    scope.endless = this.pageYOffset === 0 ? val : val + cal;
                    scope.$apply();
                }
            });
            angular.element($window).bind('resize', function () {
                scope.hh  = $window.innerHeight;
                scope.$apply();
            })
        },
        templateUrl: 'wp-content/themes/watergular/partials/header.main.html'
    };
}]);