site.directive('weatherBreeze', ['getLang', 'wpjson', '$document', function (getLang, wpjson, $document) {
    var lang = getLang.param();
    return {
        restrict: 'A',
        scope: {
            navigationMenu: '@'
        },
        link: function (scope, element, attributes) {
            wpjson.getWeather().then(function (res) {
                if (res !== null && typeof res.sys !== 'undefined') {
                    var now = res.now * 1000;
                    var dN = 'day';
                    var sunrise = res.sys.sunrise * 1000;
                    var sunset = res.sys.sunset * 1000;

                    if (new Date(now) > new Date(sunrise) && new Date(now) < new Date(sunset)) dN = 'day';
                    else dN = 'night';
                    scope.weatherIcon = getWeatherIcon(res.weather[0].id, dN);
                    scope.temp = res.main.temp;
                    scope.city = res.name;
                    scope.weekday = translations.savaitesDienos[new Date(res.dt * 1000).getDay()];
                } else {
                    scope.temp = 'N/A';
                }
            }, function () {
                scope.temp = 'N/A';
                console.log('Unsuccessful API request!');
            });
            wpjson.getWeatherWeek().then(function (res) {
                var temp = [];
                if(typeof res.list !== 'undefined') {
                    var wList = res.list;
                    for (var i = 0; i < wList.length; i++) {
                        temp.push({
                            day: translations.savaitesDienosTrump[new Date(wList[i].dt * 1000).getDay()],
                            tempDay: wList[i].temp.day,
                            tempNight: wList[i].temp.night,
                            wind: wList[i].speed,
                            humidity: wList[i].humidity,
                            icon: getWeatherIcon(wList[i].weather[0].id, 'day')
                        });
                    }
                }
                scope.items = temp;
            },function () {
                console.log('Unsuccessful API request!');
            });
            scope.show = false;

            element.on('click', function (e) {
                e.stopPropagation();
            });

            element.on('click', function () {
                scope.show = !scope.show;
                if (scope.show) element.addClass('active');
                else element.removeClass('active');
                scope.$apply();
            });

            element.find('.weather-week-wrap').on('click', function (e) {
                e.stopPropagation();
            });

            $document.find('.lang-active').on('click', function () {
                scope.show = false;
                element.removeClass('active');
                scope.$apply();
            });

            $document.on('click', function () {
                scope.show = false;
                element.removeClass('active');
                scope.$apply();
            });
        },
        templateUrl: 'wp-content/themes/watergular/partials/weather.html'
    };

    function getWeatherIcon(wId, dayTime) {
        if (wId >= 200 && wId < 300) { // Thunderstorm
            return 'thunder';
        } else if (wId >= 300 && wId < 400) { //Drizzle
            return 'drizzle';
        } else if (wId >= 500 && wId < 600) { //Rain
            return 'rain';
        } else if (wId >= 600 && wId < 700) { //Snow
            return 'snow';
        } else if (wId >= 700 && wId < 800 ) { //Fog
            return 'fog';
        } else if (wId === 800 || wId === 951) { //Clear
            return 'clear-' + dayTime;
        } else if (wId === 801 || wId === 802) { // Bit cloudy
            return 'cloudy-' + dayTime;
        } else if (wId > 802 && wId < 900) { //Cloudy
            return 'cloudy';
        } else if (wId === 903) { //Cold
            return 'cold';
        } else if (wId === 904) { //Hot
            return 'hot';
        } else if (wId > 900) { //Windy
            return 'windy';
        }
        return '';
    }
}]);