site.directive('contactForm', ['getLang', 'wpjson', '$window', function (getLang, wpjson, $window ) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            $('.email-inp').on('keyup', function () {
                if(emailValid( $(this).val())) $(this).css('border-color', '#08708A');
                else $(this).css('border-color', '#d73a31');
            });

            $('.name-inp, .subj-inp, .msg-inp').on('keyup', function () {
                if(textValid( $(this).val())) $(this).css('border-color', '#08708A');
                else $(this).css('border-color', '#d73a31');
            });

            scope.formValid = function () {
                scope.doneMSG = '';
                if(!textValid($('.name-inp').val())){
                    $('.name-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['trukstaVardo'];
                    return false;
                }
                if(!emailValid($('.email-inp').val())) {
                    $('.email-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['neteisingasElpastas'];
                    return false;
                }

                if(!textValid($('.subj-inp').val())){
                    ('.subj-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['trukstaTemos'];
                    return false;
                }

                if(!textValid($('.msg-inp').val())){
                    $('.msg-inp').css('border-color', '#d73a31');
                    scope.errorMSG = translations['trukstaZinutes'];
                    return false;
                }
                scope.errorMSG = null;
                grecaptcha.execute();
            };
            scope.submitMsg = function (token) {
                wpjson.sendEmail({name: $('.name-inp').val(), email: $('.email-inp').val(), subject: $('.subj-inp').val(), msg: $('.msg-inp').val()}).then(function (data) {
                    if(data){
                        $('.name-inp').val('');
                        $('.email-inp').val('');
                        $('.subj-inp').val('');
                        $('.msg-inp').val('');
                        scope.errorMSG = '';
                        scope.doneMSG = translations['sekmingaiNusiusta'];
                    } else {
                        scope.errorMSG = translations['nesekmingaiNusiusta'];
                    }

                }, function (err) {
                    console.log(err);
                    scope.errorMSG = translations['nesekmingaiNusiusta'];
                });
            };
            $window.submitMsg = scope.submitMsg;

            function textValid(value) {
                return value.trim().length > 0;
            }
            function emailValid(value) {
                return (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value);
            }

        },
        templateUrl: 'wp-content/themes/watergular/partials/contact.form.html'
    };
}]);