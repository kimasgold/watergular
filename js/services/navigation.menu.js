site.directive('navigationMenu', ['getLang', 'wpjson', '$document', function (getLang, wpjson, $document ) {
    var lang = getLang.param();
    return {
        restrict: 'A',
        scope: {
            navigationMenu: '@'
        },
        link: function (scope, element, attributes) {
            scope.menuList = [];
            scope.lang = lang;
            scope.homeUrl = location.origin + '/';
            wpjson.getMenus().then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].slug === lang + '-' + attributes.navigationMenu) {
                        wpjson.getMenu(data[i].ID).then(function (item) {
                            item.items.push({
                                title: translations.renginiai,
                                object: 'e',
                                object_slug: 'e/' +translations.renginiai.toLowerCase()+'?pg=1',
                                url: '/e/' +translations.renginiai.toLowerCase()+'?pg=1'
                            });
                            item.items.push({
                               title: translations.naujienos,
                                object: 'n',
                                object_slug: 'n/' + translations.naujienos.toLowerCase()+'?pg=1',
                                url: '/n/' + translations.naujienos.toLowerCase()+'?pg=1'
                            });
                            item.items.push({
                                title: translations.kontaktai,
                                object: 'k',
                                object_slug: 'k/' +translations.kontaktai.toLowerCase(),
                                url: '/k/' +translations.kontaktai.toLowerCase()
                            });
                            scope.menuList = item.items;
                        }, function () {
                        });
                        break;
                    }
                }
            }, function () {
            });
        },
        templateUrl: 'wp-content/themes/watergular/partials/menu.toolbar.html'
    };
}]);