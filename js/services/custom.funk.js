// Pop Image plugin ===================================================================================================
function addaptImages(elem) {
    var imgList = $(elem).find("img");
    for (var i = 0; i < imgList.length; i++) {
        if ($(imgList[i]).parent().is('figure')) {
            $(imgList[i]).parent().addClass('wp-image');
        } else {
            $(imgList[i]).wrap("<figure></figure>");
            $(imgList[i]).parent().addClass('wp-image');
        }
        if ($(imgList[i]).parent().parent().is('p')) $(imgList[i]).parent().unwrap();
        $(imgList[i]).parent().find('figcaption').addClass('wp-caption-text')
        $(imgList[i]).removeAttr('srcset')
    }
    for(var f = 0; f < imgList.length; f++) {
        if($(imgList[f]).parent().next().is('figure.wp-image') || $(imgList[f]).parent().prev().is('figure.wp-image')){
            $(imgList[f]).addClass('small-image');
        }
        if($(imgList[f]).parent().parent().is('a')) $(imgList[f]).parent().unwrap();
    }
    $(elem + ' img').on('click', function () {
        var el = $(this);
        var temp = this.src.split('/');
        temp[temp.length - 1] =getFullImageName(temp[temp.length - 1]);
        var link = temp.join('/');
        var wrap = $('<div class="pop-image">');
        var img = $('<img>');
        img.attr('src', link);
        img.attr('width', this.width);
        img.attr('height', this.height);
        wrap.append('<span class="close-pop-image">&times;</span>');
        wrap.append(img);
        $('body').append(wrap);

        img.on('load', function () {
            wrap.css({
                'top': getOffset(el).top + 'px',
                'left': getOffset(el).left + 'px',
                'width': el.width(),
                'height': el.height(),
                'opacity': 0.2
            });
            wrap.animate({
                'top': 0,
                'left': 0,
                'bottom': 0,
                'right': 0
            }, 200, function () {
                picResize(wrap[0]);
                img.css({
                    'max-width': img[0].naturalWidth,
                    'max-height': img[0].naturalHeight
                });

                wrap.animate({
                    'width': '100%',
                    'height': '100%',
                    'opacity': 1
                }, 300, function () {
                    $('body').addClass('fullscreen');
                    wrap.addClass('full-pop');
                    wrap.removeAttr('style');
                    picResize(wrap[0]);

                });

            });

        });
        $('.close-pop-image').on('click', function () {
            $('.pop-image').remove();
            $('body').removeClass('fullscreen');
        })
    });
    $('.gallery').on('click', function () {
        galleryIterate(this);
    });
    $('.wp-image').on('click', function (e) {
        e.preventDefault();
    });
    $(window).on('resize', function () {
        picResize($('.pop-image')[0])
    })
}

function picResize(wrap) {
    if (typeof wrap === 'object') {
        var img = $(wrap).find('img')[0];
        $(img).removeClass('high-image').removeClass('wide-image');

        var imgW = img.naturalWidth;
        var imgH = img.naturalHeight;
        var wrapW = wrap.clientWidth;
        var wrapH = wrap.clientHeight;

        // wide image corrections
        if (imgH < imgW && imgH >= wrapH && imgW < wrapW) $(img).addClass('high-image');
        else if (imgH < imgW && imgH < wrapH && imgW >= wrapW) $(img).addClass('wide-image');
        else if (imgH < imgW && imgH >= wrapH && imgW >= wrapW && wrapW <= wrapH) $(img).addClass('wide-image');
        else if (imgH < imgW && imgH >= wrapH && imgW >= wrapW && wrapW > wrapH) $(img).addClass('high-image');
        // extra width correction
        if (img.clientWidth > wrapW) $(img).removeClass('high-image').addClass('wide-image');

        // height image corrections
        if (imgH > imgW && imgH >= wrapH && imgW < wrapW) $(img).addClass('wide-image');
        else if (imgH > imgW && imgH < wrapH && imgW >= wrapW) $(img).addClass('high-image');
        else if (imgH > imgW && imgH >= wrapH && imgW >= wrapW && wrapW <= wrapH) $(img).addClass('high-image');
        else if (imgH > imgW && imgH >= wrapH && imgW >= wrapW && wrapW > wrapH) $(img).addClass('wide-image');
        // extra height correction
        if (img.clientHeight > wrapH) $(img).removeClass('wide-image').addClass('high-image');
    }
}

function getOffset(el) {
    el = el[0].getBoundingClientRect();
    return {
        left: el.left,
        top: el.top
    }
}


// Resizer ================================================================================

$(window).on('resize', function () {
    tableResize();
    iframeResize();
});


function tableResize() {
    var tb = $('table');
    for (var i = 0; i < tb.length; i++){
        if(!$(tb[i]).prev().is('div.responsive-table') && !$(tb[i]).parent().is('div.responsive-table')){
            generateSmallTable(tb[i])
        }
        if ($(tb[i]).width() > $('.re-frame').width() && $(tb[i]).prev().is('div.responsive-table')){
            $(tb[i]).prev().addClass('visible');
            $(tb[i]).hide();
        } else if ($(tb[i]).width() <= $('.re-frame').width() && $(tb[i]).prev().is('div.responsive-table')){
            $(tb[i]).prev().removeClass('visible');
            $(tb[i]).show();
        }
    }
}

function generateSmallTable(el) {
    $('<div class="responsive-table"></div>').insertBefore( el );
    var heads = $(el).find('th');
    var rows = $(el).find('tbody tr');
    for (var i = 0; i < rows.length; i++){
        var et = $(rows[i]).find('td');
        var rws = '<table><tbody>';
        for (var r = 0; r < et.length; r++){
            rws += '<tr>';
            if(r < heads.length && heads.length !== 0) rws += '<td class="first-column">' + $(heads[r]).html() + '</td>';
            rws += '<td class="second-column">' + $(et[r]).html() + '</td>';
            rws += '</tr>';
        }
        rws += '</tbody></table>';
        $('.responsive-table').append(rws);
    }
}

function iframeResize() {
    var frame = $('.re-frame').find('iframe');
    for (var i = 0; i < frame.length; i++){
        var w = $(frame[i]).attr('width');
        var h = $(frame[i]).attr('height');
        var win = $(frame[i]).parent().width();
        $(frame[i]).parent().css({'text-align': 'center'});
        if (win > 950) win = 950;
        $(frame[i]).css({'width': win+'px', 'height': (h*(win/w)) + 'px' })
    }
}

function panoramaViewer() {
    var imgList = $('.panorama360');

    for (var i = 0; i < imgList.length; i++) {
        var imgEl = $(imgList[i]).hasClass('wp-image') ? $(imgList[i]).find('img') : $(imgList[i]);
        var parent = $(imgList[i]).hasClass('wp-image') ? $(imgList[i]) : $(imgList[i]).parent();

        var img = getFullImageName(imgEl.attr('src'));
        imgEl.remove();
        parent.css('width', '100%');
        parent.prepend('<div class="loading-blob"></div>');
        parent.prepend('<div id="sphere-view-'+i+'" class="panorama-view" style="height: 500px; width:100%;"></div>');
        var itemIdx = ''+i;
        var sph = new PhotoSphereViewer({
            panorama: img,
            container: document.getElementById('sphere-view-' + itemIdx),
            time_anim: 3000,
            anim_speed: '1rpm',
            navbar: true,
            loading_msg: '',
            navbar_style: {
                backgroundColor: 'rgba(86, 177, 191, 0.7)',
                autorotateThickness: 2,
                zoomRangeThickness: 2
            },
            onready: function () {
                // parent.find('.loading-blob').hide();
            }
        });
    }
}

function getFullImageName(nm){
    try {
        var sufix = nm.match(/\.(jpg|png|gif|jpeg|bmp|JPG|PNG|GIF|JPEG|BMP)$/gm);
        return nm.replace(/-\d{2,4}x\d{2,4}\.(jpg|png|gif|jpeg|bmp|JPG|PNG|GIF|JPEG|BMP)$/gm, sufix[0]);
    } catch (e) {
        return nm;
    }
}