site.factory('getLang', ['$location', '$cookies', function ($location, $cookies) {
    return {
        param: function () {
            return $cookies.get('nkrp_langCookie') || 'lt';
        },
        set: function (lg) {
            var expireDate = new Date();
            expireDate.setFullYear(expireDate.getFullYear() + 1);
            $cookies.put('nkrp_langCookie', lg, {'expires': expireDate});
        }
    };
}]);

site.factory('linker', ['$location', function ($location) {
    return {
        slugify: function (str) {
            return str.trim().replace(/[^\w\s]/gi, '').replace(' ', '-').toLowerCase();
        },
        changeTitle: function (str) {
            var til = document.title.split('|');
            if (str && str.length > 0) document.title = str + ' | ' + til[til.length-1];
            else document.title = til[til.length-1];
        },
        getUriParam: function (key) {
            var results = new RegExp('[\?&]' + key + '=([^&#]*)').exec(window.location.href);
            try {
                return results[1];
            }
            catch (err) {
                return 'undefined';
            }
        },
        setUriParam: function (key, value) {
            key = encodeURI(key); value = encodeURI(value);
            $location.search(key, value);
        }
    };
}]);

site.factory('format', [function () {
    var form = {};
    form.dateFormat = function (date) {
        var dd = new Date(date);
        var dayOfWeek = translations.savaitesDienos[dd.getDay()];
        var month = translations.menesiai[dd.getMonth()];
        return dayOfWeek + ', ' + dd.getFullYear() + ' ' + month + ' ' + dd.getDate() + ' d. ' + ('0' + dd.getHours()).substr(-2) + ':' + ('0' +dd.getMinutes()).substr(-2);
    };
    form.eventDateFormat = function (date) {
        var dd = new Date(date);
        var dayOfWeek = translations.savaitesDienos[dd.getDay()];
        var month = translations.menesiai[dd.getMonth()];
        return dayOfWeek + ', ' + dd.getFullYear() + ' ' + month + ' ' + dd.getDate() + ' d.';
    };
    return form;
}]);

site.directive('resizeThis',  ['$window', function($window) {
    return {
        link: function(scope, elem, attrs) {
            scope.onResize = function() {
                var h = $(elem)[0].clientHeight;
                var w = $(elem)[0].clientWidth;
                var hi = $(elem).find('img')[0].clientHeight;
                var wi = $(elem).find('img')[0].clientWidth;
                // $(elem).removeClass('width-max').removeClass('height-max');
                $(elem).addClass('width-max');
                // if(w > h && wi >= hi) $(elem).addClass('width-max');
                // else if(w > h && wi < hi) $(elem).addClass('height-max');
                // else if(w < h && wi >= hi) $(elem).addClass('height-max');
                // else if(w < h && wi < hi) $(elem).addClass('width-max');
                if(wi< w)$(elem).addClass('width-max').removeClass('height-max');
                if(hi< h)$(elem).addClass('height-max').removeClass('width-max');
            };
            scope.onResize();

            angular.element($window).bind('resize', function() {
                scope.onResize();
            });

            elem.find('img').bind('load', function () {
                scope.onResize();
            });
        }
    }
}]);

site.directive('imageOnload',['$window', function($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.one('load', function() {
                scope.imgLoadedCallback(element)
            });
            element.on('activeresize', function() {
               scope.imgLoadedCallback(element);
            });
        }
    };
}]);