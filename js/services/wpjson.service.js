site.factory('wpjson', ['$http', '$q', function ($http, $q) {
    var wpjson = {};
    var API_URL = location.origin + '/wp-json/';
    //************** Pages ***********************
    wpjson.getPage = function (slug) {
        var deferred = $q.defer();
        $http.get(location.origin + '/wp-json/wp/v2/pages/?slug=' + slug).then(function (res) {
            deferred.resolve(res.data[0]);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Posts ***********************
    wpjson.getPosts = function (cat, count) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/categories/?slug=' + cat).then(function (res) {
            $http.get(API_URL + 'wp/v2/posts/?categories=' + res.data[0].id + '&per_page=' + count).then(function (res) {
                deferred.resolve(res.data);
            }, function (err) {
                deferred.reject(err);
            });
        });
        return deferred.promise;
    };

    wpjson.getPost = function (slug) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/posts/?slug=' + slug).then(function (res) {
            deferred.resolve(res.data[0]);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getPostsInPage = function (cat, page) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/categories/?slug=' + cat).then(function (res) {
            $http.get(API_URL + 'wp/v2/posts/?categories=' + res.data[0].id + '&per_page=10&page=' + page).then(function (res) {
                deferred.resolve({posts: res.data, totalPages: parseInt(res.headers('X-WP-TotalPages'))});
            }, function (err) {
                deferred.reject(err);
            });
        });
        return deferred.promise;
    };

    wpjson.searchQuery = function (query, page) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp/v2/posts/?search=' + query).then(function (res) { //+ '&per_page=10&page=' + page
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Menus ***********************
    wpjson.getMenus = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp-api-menus/v2/menus').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getMenu = function (id) {
        var deferred = $q.defer();
        $http.get(API_URL + 'wp-api-menus/v2/menus/' + id).then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Featured Image ***********************
    wpjson.getFeaturedImage = function (obj, type) {
        var deferred = $q.defer();
        if (typeof obj !== 'undefined') {
            var id = obj.featured_media;
            var idx = obj.id;
            if (typeof obj.featured_media !== 'undefined' && obj.featured_media !== 0) {
                $http.get(API_URL + 'wp/v2/media/' + id).then(function (res) {
                    if (res.data.length > 0) deferred.resolve(undefined);
                    else if (type === 'thumb' && typeof res.data.media_details.sizes.thumbnail !== 'undefined') deferred.resolve({
                        url: res.data.media_details.sizes.thumbnail.source_url,
                        id: idx,
                        width: res.data.media_details.sizes.thumbnail.width,
                        height: res.data.media_details.sizes.thumbnail.height
                    });
                    else if (type === 'medium' && typeof res.data.media_details.sizes.medium !== 'undefined') deferred.resolve({
                        url: res.data.media_details.sizes.medium.source_url,
                        id: idx,
                        width: res.data.media_details.sizes.medium.width,
                        height: res.data.media_details.sizes.medium.height

                    });
                    else deferred.resolve({
                            url: res.data.source_url,
                            id: idx,
                            width: res.data.media_details.width,
                            height: res.data.media_details.height
                        });
                }, function (err) {
                    deferred.reject(err);
                });
            } else {
                deferred.reject();
            }
        } else {
            deferred.reject();
        }
        return deferred.promise;
    };
    //************** Weather ***********************
    wpjson.getWeather = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'breeze/v1/today').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getWeatherWeek = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'breeze/v1/week').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    //************** Gallery ***********************

    wpjson.getAlbums = function () {
        var deferred = $q.defer();
        $http.get(API_URL + 'slash/v2/albums').then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    wpjson.getGallery = function (id) {
        var deferred = $q.defer();
        $http.get(API_URL + 'slash/v2/album/'+id).then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    //************** Email form ***********************
    wpjson.sendEmail = function (obj) {
        var deferred = $q.defer();
        $http.post(API_URL + 'email/v1/send', obj).then(function (res) {
            deferred.resolve(res.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };


    return wpjson;
}]);