site.directive('mainFooter',  ['$window', 'wpjson', 'getLang', function($window, wpjson, getLang) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            scope.copy = new Date().getFullYear();
            scope.links = '';
            scope.contacts = '';
            scope.errorNF = '';
            scope.doneNF = '';
            wpjson.getPage('nuorodos').then(function (data) {
                scope.links = data;
            });
            wpjson.getPage( getLang.param() + '-kontaktai-apacia').then(function (data) {
                scope.contacts = data;
            });

        },
        templateUrl: 'wp-content/themes/watergular/partials/footer.html'
    };
}]);

site.directive('footerMenu',  ['$window', 'wpjson', 'getLang', function($window, wpjson, getLang) {
    var lang = getLang.param();
    return {
        restrict: 'A',
        scope: {
            menuTitle: '@'
        },
        link: function (scope, element, attributes) {
            scope.menuList = [];
            scope.lang = lang;
            wpjson.getMenus().then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].slug === lang + '-' + attributes.menuTitle) {
                        wpjson.getMenu(data[i].ID).then(function (item) {
                            scope.menuList = item.items;
                        }, function () {
                        });
                        break;
                    }
                }
            }, function () {});
        },
        templateUrl: 'wp-content/themes/watergular/partials/footer.menu.html'
    };
}]);

