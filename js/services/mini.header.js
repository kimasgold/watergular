site.directive('miniHeader',  ['$window', function($window) {
    return {
        restrict: 'A',
        scope: {
            headerImg: '@'
        },
        link: function (scope, element, attributes) {
            attributes.$observe('headerImg', function (src) {
                scope.imgSrc = src;
            });

            var val = 0; // -30
            scope.hh  = $window.innerHeight;
            scope.endless = val;
            angular.element($window).bind("scroll", function(event) {
                var offset = this.pageYOffset;
                if(document.querySelector('.small-header-img') !== null && offset > 0){
                    var cal = (80 * offset-$window.innerHeight)/$window.innerHeight;
                    scope.endless = cal <= val ? val : val + cal;
                    scope.$apply();
                }
            });
            angular.element($window).bind("resize", function() {
                var head = $('.small-header-img');
                var w = head.find('img').width();
                var wf = head.width();
                var h = head.find('img').height();
                var hf = head.height();
            })

        },
        templateUrl: 'wp-content/themes/watergular/partials/header.small.html'
    };
}]);