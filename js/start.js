//Global Variables
var translations = {};
var translationsMemory = {};
var langs = [];
//AngularJS init
var site = angular.module('wpgular', ['ngCookies','ngRoute', 'ngSanitize', 'ngAnimate']);

site.config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: myLocalized.partials + 'main.html',
            controller: 'mainCtrl'
        })
        .when('/p/:slug', {
            templateUrl: myLocalized.partials + 'page.html',
            controller: 'pageCtrl'
        })
        //Galerija ------------------------------------------------
        .when('/g', {
            templateUrl: myLocalized.partials + 'gallery.html',
            controller: 'galleryCtrl'
        })
        .when('/g/:slug', {
            templateUrl: myLocalized.partials + 'gallery.html',
            controller: 'galleryCtrl'
        })
        //School ------------------------------------------------
        .when('/s', {
            templateUrl: myLocalized.partials + 'school.html',
            controller: 'schoolCtrl'
        })
        .when('/s/:slug', {
            templateUrl: myLocalized.partials + 'school.html',
            controller: 'schoolCtrl'
        })
        //Places ------------------------------------------------
        .when('/v', {
            templateUrl: myLocalized.partials + 'places.html',
            controller: 'placesCtrl'
        })
        .when('/v/:slug', {
            templateUrl: myLocalized.partials + 'places.html',
            controller: 'placesCtrl'
        })
        //Kontaktai ----------------------------------------------
        .when('/k/:slug', {
            templateUrl: myLocalized.partials + 'contacts.html',
            controller: 'contactsCtrl'
        })
        //News ----------------------------------------------------
        .when('/n/:slug', {
            templateUrl: myLocalized.partials + 'news.html',
            controller: 'newsCtrl'
        })
        //Events ----------------------------------------------------
        .when('/e/:slug', {
            templateUrl: myLocalized.partials + 'events.html',
            controller: 'eventsCtrl'
        })
        //Search ----------------------------------------------------
        .when('/q', {
            templateUrl: myLocalized.partials + 'search.html',
            controller: 'searchCtrl'
        })
        //Posts -----------------------------------------------------
        .when('/:slug', {
            templateUrl: myLocalized.partials + 'post.html',
            controller: 'postCtrl'
        })
        .otherwise({
            redirectTo: "/"
        });
});
site.run(function ($rootScope, $window, $http, getLang) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        $window.scrollTo(0, 0)
    });
    $http.get('wp-content/themes/watergular/i18n/translations.json').then(function (res) {
        translationsMemory = res.data;
        translations = $rootScope.translation = res.data[getLang.param()];
        for (var key in res.data) {
            if (res.data.hasOwnProperty(key)) langs.push(key);
        }
        $rootScope.social = {
            title: translations.title,
            description: '',
            url: $window.location.href,
            img:'../img/wall2.jpg'
        }
    }, function (err) {
    });
});