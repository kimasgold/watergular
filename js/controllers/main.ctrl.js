site.controller('mainCtrl', ['$scope', 'wpjson', 'getLang', 'linker', 'format', '$window', '$sce', '$rootScope',
    function ($scope, wpjson, getLang, linker, format, $window, $sce, $rootScope) {
        $rootScope.sideBar = false;
        var currentLang = 'lt'; //getLang.param()

        linker.changeTitle('');
        //events section =====================================
        $scope.events = [];
        wpjson.getPosts( currentLang + '-renginiai', 3).then(function (data) {
            $scope.events = reorderEvents('date_select', data);
            for (var i = 0; i < $scope.events.length; i++) {
                $scope.events[i].more = location.origin + '/' + $scope.events[i].slug;
                $scope.events[i].date = format.eventDateFormat($scope.events[i].date_select);
                wpjson.getFeaturedImage($scope.events[i], 'thumb').then(function (img) {
                    for (var j = $scope.events.length - 1; j >= 0; j--) {
                        if (img && $scope.events[j].id === img.id) {
                            $scope.events[j].eventImage = img.url;
                            break;
                        }
                    }
                }, function () {
                })
            }
        }, function () {
        });

        function reorderEvents(key, data) {
            var temp = [];
            var today = new Date().setHours(0, 0, 0, 0);

            for (var i = 0; i < data.length; i++) {
                var dd = new Date(data[i][key]).getTime();
                if (today === dd) data[i].period = 'today';
                else if (today < dd) data[i].period = 'future';
                else data[i].period = 'past';
                for (var j = 0; j < temp.length; j++) {
                    var dt = new Date(temp[j][key]).getTime();
                    if (dd <= dt) {
                        temp.splice(j, 0, data[i]);
                        break;
                    } else if (j === temp.length - 1) {
                        temp.push(data[i]);
                        break;
                    }
                }
                if (temp.length === 0) {
                    temp.push(data[i]);
                }
            }
            return temp.reverse();
        }

        // news section ======================================
        $scope.newsLink = '/n/' + translations.naujienos.toLowerCase() + '?pg=1';
        $scope.posts = null;
        wpjson.getPosts(currentLang + '-naujienos', 3).then(function (data) {
            $scope.posts = data;
            for (var i = 0; i < data.length; i++) {
                $scope.posts[i].more = location.origin + '/' + data[i].slug;
                wpjson.getFeaturedImage(data[i], 'medium').then(function (img) {
                    for (var j = $scope.posts.length - 1; j >= 0; j--) {
                        if (img && $scope.posts[j].id === img.id) {
                            $scope.posts[j].newsImage = img.url;
                            $scope.posts[j].newsImageSize = {width: img.width, height: img.height};
                            break
                        }
                    }
                }, function () {
                })
            }

        }, function () {
        });
        $scope.resp = false;
        newsSizes();
        $(window).on('resize', newsSizes);
        function newsSizes() {
            $scope.resp = $(window).width() < 900;
        }

        // boxes ==============================================
        boxSizes();
        $(window).on('resize', boxSizes);
        $scope.gallerylink = '/g/' + linker.slugify(translations.galerija);
        $scope.naturelink = '/s/' + linker.slugify(translations.gamtosMokykla);
        $scope.placeslink = '/v/' + linker.slugify(translations.lankytinosVietos);
        $scope.productBrand = '/p/produkto-zenklas';
        $scope.shownatureschool = getLang.param() === 'lt';
        $scope.showproductbrand = getLang.param() === 'lt';
        function boxSizes() {
            var ww = $(window).width();
            var wd = (ww * 0.7); // 0.7 -> 70%
            if (ww > 1200) wd = 1200 * 0.7;
            var wh = wd / 3;
            if (ww < 600) $('.pop-box').width('100%').height($('.pop-box').width());
            else $('.pop-box').width(wh).height(wh);
            $('.boxes-wrapper').css('top', ((wh / 3) * -1) + 'px');

        }
        // write to us =========================================

    }]);

