site.controller('newsCtrl', ["$scope", "wpjson", "$routeParams", "linker", 'getLang', 'format', "$rootScope", function ($scope, wpjson, $routeParams, linker, getLang, format,$rootScope) {
    linker.changeTitle(translations.naujienos);
    $scope.featuredImage = '';
    $scope.posts = [];
    $scope.pages = [];
    $rootScope.sideBar = false;
    var currentPage = linker.getUriParam('pg');
    $scope.currentPage = currentPage !== 'undefined' ? parseInt(currentPage) : 1;
    jumpToThisPage($scope.currentPage);
    $scope.jumpToPage = function (itm) {
        switch (itm) {
            case 'first':
                jumpToThisPage(1);
                break;
            case 'prev':
                jumpToThisPage($scope.currentPage - 1);
                break;
            case 'next':
                jumpToThisPage($scope.currentPage + 1);
                break;
            case 'last':
                jumpToThisPage($scope.pages.length);
                break;
            default:
                if (itm !== $scope.currentPage) jumpToThisPage(itm);
                break;
        }
    };
    function jumpToThisPage(pg) {
        $scope.posts = [];
        $scope.pages = [];
        var currentLang = 'lt'; //getLang.param()
        wpjson.getPostsInPage(currentLang + '-naujienos', pg).then(function (data) {
            $scope.posts = data.posts;
            $scope.currentPage = pg;

            for (var p = 0; p < data.totalPages; p++) $scope.pages.push(p + 1);

            for (var i = 0; i < data.posts.length; i++) {
                $scope.posts[i].more = location.origin + '/' + data.posts[i].slug;
                $scope.posts[i].date = format.dateFormat(data.posts[i].date);
                wpjson.getFeaturedImage(data.posts[i], 'thumb').then(function (img) {
                    for (var j = data.posts.length - 1; j >= 0; j--) {
                        if(j === 0) $scope.featuredImage = img.url.replace('-'+img.width+'x'+img.height, '');
                        else if ($scope.featuredImage.length === 0) $scope.featuredImage = img.url.replace('-'+img.width+'x'+img.height, '');
                        if (img && data.posts[j].id === img.id) {
                            $scope.posts[j].newsImage = img.url;
                            break;
                        }
                    }
                })
            }
            linker.setUriParam('pg', pg)
        })
    }
}]);