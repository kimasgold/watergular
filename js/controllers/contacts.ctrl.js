site.controller('contactsCtrl', ["$scope", "wpjson", "$routeParams", 'getLang', 'linker', '$rootScope', function ($scope, wpjson, $routeParams, getLang, linker, $rootScope) {
    linker.changeTitle(translations.kontaktai);
    $rootScope.sideBar = false;

    wpjson.getPage(getLang.param() + '-kontaktai').then(function (data) {
        $scope.page = data;
        $scope.$watch('page', function () {
            addaptImages('.page-text-render');
            tableResize();
        });
        wpjson.getFeaturedImage(data).then(function (img) {
            $scope.featuredImage = img.url;
        }, function () {
        })
    });

    var overlay = $('.map-contact .overlay');
    overlay.show();
    overlay.on('click', function () {
        overlay.hide();
    });

    $('.map-contact iframe').on('mouseleave', function () {
        overlay.show();
    });
}]);