site.controller('postCtrl', ["$scope", "wpjson", "$routeParams", "format", "linker", "$sce", '$rootScope', function ($scope, wpjson, $routeParams, format, linker, $sce, $rootScope) {
    $rootScope.sideBar = false;
    wpjson.getPost($routeParams.slug).then(function (data) {
        data.date = format.dateFormat(data.date);

        $scope.post = data;
        $scope.trusted = $sce.trustAsHtml(data.content.rendered.replace(/<a\shref=/g, '<a target="_blank" href='));
        linker.changeTitle(data.title.rendered);
        $scope.fbShare = '//www.facebook.com/sharer/sharer.php?u=' + location.href;
        $scope.twShare = '//twitter.com/home?status=' + location.href.replace(':', '%3A');
        $scope.gpShare = '//plus.google.com/share?url=' + 'google.lt';
        $scope.$watch('post', function () {
            addaptImages('.post-text-render');
            tableResize();
            iframeResize();
            panoramaViewer();
        });
        wpjson.getFeaturedImage(data).then(function (img) {
            $scope.featuredImage = img.url;
        })
    });
}]);