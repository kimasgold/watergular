site.controller('searchCtrl', ['$scope', 'wpjson', "$routeParams", 'getLang', 'linker', '$rootScope', 'format', function ($scope, wpjson, $routeParams, getLang, linker, $rootScope, format) {
    var queryParam = location.search || '';
    if (queryParam.length > 0) queryParam = decodeURIComponent(queryParam).split("?")[1].split("&")[0].replace('query=', '');

    linker.changeTitle(translations.paieska);
    $scope.featuredImage = './wp-content/themes/watergular/img/headers/search.jpg';
    $scope.posts = [];
    $scope.pages = [];
    $scope.query = queryParam.replace('+', ' ');
    $rootScope.sideBar = false;
    var currentPage = linker.getUriParam('pg');
    $scope.currentPage = currentPage !== 'undefined' ? parseInt(currentPage) : 1;

    if (queryParam.trim().length > 0) {
        jumpToThisPage($scope.currentPage);
        $scope.jumpToPage = function (itm) {
            switch (itm) {
                case 'first':
                    jumpToThisPage(1);
                    break;
                case 'prev':
                    jumpToThisPage($scope.currentPage - 1);
                    break;
                case 'next':
                    jumpToThisPage($scope.currentPage + 1);
                    break;
                case 'last':
                    jumpToThisPage($scope.pages.length);
                    break;
                default:
                    if (itm !== $scope.currentPage) jumpToThisPage(itm);
                    break;
            }
        };

        function jumpToThisPage(pg) {
            $scope.posts = [];
            $scope.pages = [];
            wpjson.searchQuery(queryParam, pg).then(function (data) {
                $scope.posts = data;
                $scope.currentPage = pg;
                $scope.finishSearch = true;
                for (var p = 0; p < data.totalPages; p++) $scope.pages.push(p + 1);
                data.forEach(function (itm, idx) {
                    if (itm.date_select) $scope.posts[idx].date = format.eventDateFormat(itm.date_select);
                    else $scope.posts[idx].date = format.dateFormat(itm.date);
                    $scope.posts[idx].more = location.origin + '/' + itm.slug;
                });

                linker.setUriParam('pg', pg)
            })
        }
    } else {
        $scope.finishSearch = true;
    }

}]);