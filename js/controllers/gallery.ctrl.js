site.controller('galleryCtrl', ['$scope', 'wpjson', 'getLang', 'linker', '$window', '$document', '$rootScope',
    function ($scope, wpjson, getLang, linker, $window, $document, $rootScope) {
        linker.changeTitle(translations.galerija);
        $scope.featuredImage = './wp-content/themes/watergular/img/boxes/galllery-min.JPG';
        $scope.albums = [];
        $scope.gallery = [];
        $scope.selected = 0;
        $rootScope.sideBar = false;
        var lang = getLang.param();
        wpjson.getAlbums().then(function (alb) {
            for (var i = 0; i < alb.length; i++) {
                var nameAr = alb[i].album_name_feed.filter(function (item) {
                    return item.lang === lang;
                });

                var name = (nameAr.length > 0 ) ? nameAr[0].name : alb[i].slug;
                $scope.albums.push({
                    id: alb[i].id,
                    name: name
                })
            }
            if (alb.length > 0) getSelectedAlbum(0, alb[0].id);
        });
        $scope.changeGallery = getSelectedAlbum;
        var galWrap = $('.gallery-render');
        var wrapWidth = galWrap.width();
        var colCount = Math.floor(wrapWidth / 250);
        var elmWidth = wrapWidth / colCount;
        var trackColumns = [];
        for (var i = 0; i < colCount; i++)trackColumns.push(0);

        function getSelectedAlbum(idx, id) {
            $scope.selected = idx;
            galWrap.height(200);
            trackColumns = [];  // reset variables
            for (var i = 0; i < colCount; i++)trackColumns.push(0);
            $scope.gallery = [];
            $scope.loadShow = true;
            wpjson.getGallery(id).then(function (gal) {
                for (var i = 0; i < gal.length; i++) {
                    var name = '';
                    if (gal[i].img_name_feed !== null) {
                        if (typeof gal[i].img_name_feed[lang] !== 'undefined') name = gal[i].img_name_feed[lang];
                    }

                    $scope.gallery.push({
                        img: gal[i].thumb,
                        name: name
                    })
                }
                $scope.loadShow = false;
            }, function () {
                $scope.loadShow = false;
            });
        }

        var resizeId;
        $($window).on('resize', function () {
            wrapWidth = galWrap.width();
            colCount = Math.floor(wrapWidth / 250);
            elmWidth = wrapWidth / colCount;
            trackColumns = [];
            for (var i = 0; i < colCount; i++) trackColumns.push(0);
            clearTimeout(resizeId);
            resizeId = setTimeout(function () {
                $('.gallery-item-thumb').trigger('activeresize');
            }, 200);
        });

        $scope.imgLoadedCallback = function (elm) {
            var margin = 6;
            var el = $(elm);
            var par = el.parent();
            var shortColumn = trackColumns.indexOf(Math.min.apply(Math, trackColumns));
            var topPos = trackColumns[shortColumn];

            var offset = (margin / 3) * 2;
            var h = (el[0].naturalHeight / el[0].naturalWidth) * (elmWidth - offset);

            el.width(elmWidth - offset).height(h);
            par.width(elmWidth - offset).height(h);
            trackColumns[shortColumn] += h + margin;

            var left = shortColumn * (elmWidth - offset);
            if (shortColumn !== 0) left += (margin * shortColumn);

            par.css({'left': left + 'px', 'top': topPos + 'px'});
            par.animate({'opacity': 1});
            galWrap.height(Math.max.apply(Math, trackColumns));
        };

        $($document).on('click', '.gallery-item-caption', function () {
            var el = $(this).parent().find('img').first();
            var link = el[0].src.replace('thumb/', '');
            var wrap = $('<div class="pop-image">');
            var img = $('<img>');
            img.attr('src', link);
            img.attr('width', this.width);
            img.attr('height', this.height);
            img.attr('data-idx', $(this).parent().index());

            var prevImg = $(this).parent().prev().first().find('img');
            var nextImg = $(this).parent().next().first().find('img');
            if (nextImg.length > 0)
                wrap.append('<span class="next-pop-image" data-imgsrc="' + nextImg[0].src.replace('/thumb', '') + '" data-idx="' + $(this).parent().next().first().index() + '"></span>');
            else
                wrap.append('<span class="next-pop-image" style="display: none"></span>');

            if (prevImg.length > 0)
                wrap.append('<span class="prev-pop-image" data-imgsrc="' + prevImg[0].src.replace('/thumb', '') + '" data-idx="' + $(this).parent().prev().first().index() + '"></span>');
            else
                wrap.append('<span class="prev-pop-image" style="display: none"></span>');

            wrap.append('<span class="close-pop-image">&times;</span>');
            wrap.append(img);
            $('body').append(wrap);
            img.on('load', function () {
                wrap.css({
                    'top': getOffset(el).top + 'px',
                    'left': getOffset(el).left + 'px',
                    'width': el.width(),
                    'height': el.height(),
                    'opacity': 0.2
                });
                wrap.animate({
                    'top': 0,
                    'left': 0,
                    'bottom': 0,
                    'right': 0
                }, 200, function () {
                    picResize(wrap[0]);
                    img.css({
                        'max-width': img[0].naturalWidth,
                        'max-height': img[0].naturalHeight
                    });

                    wrap.animate({
                        'width': '100%',
                        'height': '100%',
                        'opacity': 1
                    }, 300, function () {
                        $('body').addClass('fullscreen');
                        wrap.addClass('full-pop');
                        wrap.removeAttr('style');
                        picResize(wrap[0]);
                        img.off('load');
                    });

                });
            });
            $('.close-pop-image').on('click', function () {
                $('.pop-image').remove();
                $('body').removeClass('fullscreen');
            });
            $('.prev-pop-image').on('click', function () {
                var getSrc = $(this).data('imgsrc');
                var ind = $(this).data('idx');
                var nextBtn = $('.next-pop-image');
                //set next button
                nextBtn.show();
                var curr = $('.pop-image').find('img');
                var nextItm = curr[0].src;
                nextBtn.data('imgsrc', nextItm).data('idx', curr.data('idx'));
                //set main img
                curr.data('idx', ind);
                curr[0].src = getSrc;
                //set prev button
                var prevItem = $($('.gallery-render').find('.gallery-item').get(parseInt(ind))).prev('.gallery-item');
                if (prevItem.length > 0) {
                    $(this).data('idx', prevItem.index());
                    $(this).data('imgsrc', prevItem.find('img')[0].src.replace('/thumb', ''));
                } else {
                    $(this).hide();
                }
                curr.on('load', function () {
                    $(this).removeAttr('style');
                    picResize($('.pop-image')[0]);
                    $(this).off('load')
                });
            });
            $('.next-pop-image').on('click', function () {
                var getSrc = $(this).data('imgsrc');
                var ind = $(this).data('idx');
                var prevBtn = $('.prev-pop-image');
                //set prev button
                prevBtn.show();
                var curr = $('.pop-image').find('img');
                var prevItm = curr[0].src;
                prevBtn.data('imgsrc', prevItm).data('idx', curr.data('idx'));
                //set main img
                curr.data('idx', ind);
                curr[0].src = getSrc;
                //set next button
                var nextItem = $($('.gallery-render').find('.gallery-item').get(parseInt(ind))).next('.gallery-item');
                if (nextItem.length > 0) {
                    $(this).data('idx', nextItem.index());
                    $(this).data('imgsrc', nextItem.find('img')[0].src.replace('/thumb', ''));
                } else {
                    $(this).hide();
                }
                curr.on('load', function () {
                    $(this).removeAttr('style');
                    picResize($('.pop-image')[0]);
                    $(this).off('load')
                });

            })
        });

        $(window).on('resize', function () {
            picResize($('.pop-image')[0])
        })

    }]);