site.controller('schoolCtrl', ['$scope', 'wpjson', 'getLang', 'linker', '$rootScope', function ($scope, wpjson, getLang, linker, $rootScope) {
    $scope.title = translations.gamtosMokykla || 'Gamtos mokykla';
    linker.changeTitle($scope.title);
    $scope.featuredImage = './wp-content/themes/watergular/img/boxes/school-min.JPG';
    $rootScope.sideBar = false;
    $scope.tabs = {};
    $scope.tabContents = [];
    $scope.wid = 0;
    $scope.setted = 0;
    wpjson.getMenus().then(function (data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].slug === getLang.param() + '-gamtos-mokykla') {
                wpjson.getMenu(data[i].ID).then(function (item) {
                    $scope.tabs = item.items;
                    $scope.wid = 100 / item.items.length;

                    for (var j = 0; j < item.items.length; j++) {
                        // $scope.tabContents[j] = '';
                        wpjson.getPage(item.items[j].object_slug).then((function (pg) {
                            $scope.tabContents.push(pg);
                        }))
                    }
                }, function () {
                });
                break;
            }
        }
    }, function () {
    });

    $scope.newTab = function (id, idx) {
        $('.tab-content').hide();
        $scope.setted = idx;
        $('.tab-content.content-' + id).fadeIn('slow');
        $('.tabs-line .selector').animate({'left': idx * $scope.wid + '%'}, 'fast')
    }
}]);