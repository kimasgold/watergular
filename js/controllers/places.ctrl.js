site.controller('placesCtrl', ['$scope', 'wpjson', 'getLang', 'linker', '$rootScope', function ($scope, wpjson, getLang, linker, $rootScope) {
    linker.changeTitle(translations.lankytinosVietos );
    $scope.featuredImage = './wp-content/themes/watergular/img/boxes/places-min.jpg';
    $rootScope.sideBar = false;
    var overlay = $('.map-contact .overlay');
    overlay.show();
    overlay.on('click', function () {
        overlay.hide();
    });

    $('.map-contact iframe').on('mouseleave', function () {
        overlay.show();
    });
}]);