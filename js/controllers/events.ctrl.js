site.controller('eventsCtrl', ["$scope", "wpjson", "$routeParams", "linker", 'getLang', 'format', '$rootScope', function ($scope, wpjson, $routeParams, linker, getLang, format, $rootScope) {
    linker.changeTitle(translations.renginiai);
    $scope.featuredImage = '';
    $scope.events = [];
    $scope.pages = [];
    $rootScope.sideBar = false;
    var currentPage = linker.getUriParam('pg');
    $scope.currentPage = currentPage !== 'undefined' ? parseInt(currentPage) : 1;
    jumpToThisPage($scope.currentPage);
    $scope.jumpToPage = function (itm) {
        switch (itm) {
            case 'first':
                jumpToThisPage(1);
                break;
            case 'prev':
                jumpToThisPage($scope.currentPage - 1);
                break;
            case 'next':
                jumpToThisPage($scope.currentPage + 1);
                break;
            case 'last':
                jumpToThisPage($scope.pages.length);
                break;
            default:
                if (itm !== $scope.currentPage) jumpToThisPage(itm);
                break;
        }
    };

    function jumpToThisPage(pg) {
        $scope.events = [];
        $scope.pages = [];
        var currentLang = 'lt'; //getLang.param()
        wpjson.getPostsInPage(currentLang + '-renginiai', pg).then(function (data) {
            $scope.events = reorderEvents('date_select', data.posts);
            $scope.currentPage = pg;
            for (var p = 0; p < data.totalPages; p++) $scope.pages.push(p + 1);

            for (var i = 0; i < data.posts.length; i++) {
                $scope.events[i].more = location.origin + '/' + $scope.events[i].slug;
                $scope.events[i].date = format.eventDateFormat($scope.events[i].date_select);
                wpjson.getFeaturedImage($scope.events[i], 'thumb').then(function (img) {
                    for (var j = $scope.events.length - 1; j >= 0; j--) {
                        if (j === 0) $scope.featuredImage = img.url.replace('-' + img.width + 'x' + img.height, '');
                        else if ($scope.featuredImage.length === 0) $scope.featuredImage = img.url.replace('-' + img.width + 'x' + img.height, '');

                        if (img && $scope.events[j].id === img.id) {
                            $scope.events[j].eventImage = img.url;
                            break
                        }
                    }
                }, function () {
                })
            }
            linker.setUriParam('pg', pg)
        })
    }

    function reorderEvents(key, data) {
        var temp = [];
        var today = new Date().setHours(0, 0, 0, 0);

        for (var i = 0; i < data.length; i++) {
            var dd = new Date(data[i][key]).getTime();
            if (today === dd) data[i].period = 'today';
            else if (today < dd) data[i].period = 'future';
            else data[i].period = 'past';
            for (var j = 0; j < temp.length; j++) {
                var dt = new Date(temp[j][key]).getTime();
                if (dd <= dt) {
                    temp.splice(j, 0, data[i]);
                    break
                } else if (j === temp.length - 1) {
                    temp.push(data[i]);
                    break
                }
            }
            if (temp.length === 0) temp.push(data[i]);
        }
        return temp.reverse();
    }
}]);