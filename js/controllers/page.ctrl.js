site.controller('pageCtrl', ["$scope", "wpjson", "$routeParams", "linker", "$sce", '$rootScope', function ($scope, wpjson, $routeParams, linker, $sce, $rootScope) {
    $rootScope.sideBar = false;
    wpjson.getPage($routeParams.slug).then(function (data) {
        $scope.page = data;
        $scope.trusted = $sce.trustAsHtml(data.content.rendered.replace(/<a\shref=/g, '<a target="_blank" href='));
        linker.changeTitle(data.title.rendered);
        $scope.$watch('page', function() {
            addaptImages('.page-text-render');
            tableResize();
            iframeResize();
            panoramaViewer();
        });
        wpjson.getFeaturedImage(data).then(function (img) {
            $scope.featuredImage = img.url;
        })
    }, function () {
    });
}]);