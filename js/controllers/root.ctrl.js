site.controller('rootCtrl', ['$scope', '$rootScope', '$http', 'getLang', '$location', '$window', '$document', '$cookies',
        function ($scope, $rootScope, $http, getLang, $location, $window, $document, $cookies ) {
            $rootScope.sideBar = false;
            $scope.searchActive = false;
            $scope.searchInput = '';
            $scope.langList = langs;
            $scope.activeLang = getLang.param();
            $scope.openList = false;

            $('.lang-active').on('click', function (e) {
                $scope.openList = !$scope.openList;
                $scope.searchActive = false;
                $scope.$apply();
                e.stopPropagation();
            });
            $document.find('.weather-wrap').on('click', function () {
                $scope.openList = false;
                $scope.searchActive = false;
            });

            $scope.changeLang = function (lg) {
                getLang.set(lg);
                $window.location.href = $window.location.protocol + '//' + $window.location.host + '/';
            };

            $scope.sideBarBtn = function () {
              $rootScope.sideBar = !$rootScope.sideBar;
            };
            // search ================================

            $scope.searchBtn = function (e) {
                $scope.openList = false;
                $scope.searchActive = !$scope.searchActive;
                // e.stopPropagation();
            };

            $('.search-input-field').on('keyup', function (e) {
                if (e.keyCode === 13) {
                    var query = $scope.searchInput.trim();
                    query = query.match(/[\d|a-zA-Ząčęėįšųūž]+/gi);

                    if(query){
                        query = query.join('+');
                        $window.location.href = $window.location.protocol + '//' + $window.location.host + '/q?query=' + encodeURIComponent(query);
                    }
                }
            });



            $('.search-input, .search-input input').on('click', function (e) {
               e.stopPropagation();
            });
            // document click reset ==================
            $document.on('click', function () {
                $scope.openList = false;
            });
            // up up and go ==========================
            $($window).on('scroll', function () {
               $scope.showScrollBtn = $window.pageYOffset > $window.innerHeight;
            });

            $scope.scrollToTop = function () {
                TweenLite.to($window, 1, {scrollTo:0});
            };

            // cookies notice ========================

            $scope.showCookiesNotice = !$cookies.get('nkrp_cookiesNotice');
            $scope.cookieMsgClose =  function () {
                var expireDate = new Date();
                expireDate.setFullYear(expireDate.getFullYear() + 1);
                $cookies.put('nkrp_cookiesNotice', true, {'expires': expireDate});
                $scope.showCookiesNotice = false;
            };
        }]);