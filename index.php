<?php
$name = get_bloginfo( 'name' );
$desc = get_bloginfo( 'description' );
?>
<!DOCTYPE html>
<html>
<head>
	<base href="/">
	<title><?php echo $name; ?></title>
	  <meta charset="UTF-8">
      <meta name="description" content="<?php echo $desc; ?>">
      <meta name="keywords" content="nemunas, regioninis parkas, direkcija, lietuva, birstonas, regioninis, parkas, lankytinos vietos">
      <meta name="author" content="NKRPD">
      <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/watergular/apple-touch-icon.png">
      <link rel="icon" type="image/png" href="/wp-content/themes/watergular/favicon-32x32.png" sizes="32x32">
      <link rel="icon" type="image/png" href="/wp-content/themes/watergular/favicon-16x16.png" sizes="16x16">
      <link rel="manifest" href="/wp-content/themes/watergular/manifest.json">
      <link rel="mask-icon" href="/wp-content/themes/watergular/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="theme-color" content="#ffffff">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <!-- Twitter Card data -->
      <meta name="twitter:card" content="summary">
      <meta name="twitter:site" content="@publisher_handle">
      <meta name="twitter:title" content="<?php echo $name; ?>">
      <meta name="twitter:description" content="<?php echo $desc; ?>">
      <meta name="twitter:image" content="./wp-content/themes/watergular/img/nkrp.png">

      <!-- Open Graph data -->
      <meta property="og:title" content="<?php echo $name; ?>">
      <meta property="og:type" content="article">
      <meta property="og:image" content="./wp-content/themes/watergular/img/nkrp.png">
      <meta property="og:description" content="<?php echo $desc; ?>">

	  <?php wp_head(); ?>
	  <script type="text/javascript">var mainTitle = "<?php echo $name; ?>"</script>
</head>
<body ng-app="wpgular" ng-cloak ng-controller="rootCtrl">
    <div class="navigation-toolbar">
        <span class="menu-resp-btn" ng-click="sideBarBtn()"></span>
        <nav navigation-menu="virsus" ng-class="{'open': sideBar}"></nav>
        <div class="search-wrap" ng-class="{'search-wrap--active': searchActive}">
        <div class="search-input"><input type="text" class="search-input-field" ng-model="searchInput" placeholder="{{translation.paieska}}"></div>
            <span class="search-btn search-icon" ng-click="searchBtn($event)"></span>
        </div>
        <div weather-breeze class="weather-wrap"></div>

        <div class="social-icons-wrap">
            <a href="https://www.facebook.com/nemunokilpos/" target="_blank" class="facebook-icon"></a>
            <a href="https://www.instagram.com/nemunokilpos/" target="_blank" class="instagram-icon"></a>
        </div>
        <div class="language-selector" ng-class="{'open': openList}">
            <span class="lang-active">{{activeLang}}</span>
            <ul>
                <li class="lang-select" ng-repeat="lang in langList" ng-click="changeLang(lang);">{{lang}}</li>
            </ul>
        </div>
    </div>

    <main ng-view></main>

	<footer main-footer></footer>
	<span ng-click="scrollToTop()" class="scroll-to-top-btn" ng-class="{show : showScrollBtn}"></span>
	<div class="cookie-msg-wrap" ng-if="showCookiesNotice">
	    <span ng-bind-html="translation.cookieText"></span>
	    <button type="button" class="cookie-msg-close" ng-click="cookieMsgClose()">{{translation.supratau}}</button>
	</div>
	<span class="menu-overlay" ng-if="sideBar" ng-click="sideBarBtn()"></span>
</body>
<?php wp_footer(); ?>
</html>