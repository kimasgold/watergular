<?php
function theme_setup(){
add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );
    add_filter('show_admin_bar', '__return_false');
    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'watergular' ),
    ) );
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    // Custom logo support.
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'theme_setup' );

function site_scripts() {
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Comfortaa:400,500,600,700|Open+Sans:400,600,700,800&amp;subset=cyrillic,latin-ext', false );
    wp_enqueue_style( 'style', get_stylesheet_uri(),array(),  filemtime(  realpath(dirname(__FILE__)) .'/style.css'));

    add_theme_support( 'menus' );
    register_nav_menus(
        array(
          'header-menu' => __( 'Header Menu' ),
          'extra-menu' => __( 'Extra Menu' )
        )
      );
}
add_action('wp_enqueue_scripts',   'site_scripts');

function add_defer_attribute($tag, $handle) {
    return str_replace( ' src', ' src', $tag ); //  defer="defer"
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

function site_js() {
    	wp_enqueue_script(
    		'script',
    		get_template_directory_uri() . '/script.js',
    		array(),
    		filemtime(  realpath(dirname(__FILE__)) .'/script.js'),
    		true
    	);
    	wp_localize_script(
                'script',
            	'myLocalized',
            	array(
            		'partials' => trailingslashit( get_template_directory_uri() ) . 'partials/'
            	)
            );

}
add_action('wp_enqueue_scripts',   'site_js');

add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

add_filter('jpeg_quality', function($arg){return 82;});
?>