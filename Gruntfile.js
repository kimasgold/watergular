module.exports = function (grunt) {
    grunt.initConfig({
        concat: {
            lib: {
                src: [
                    'node_modules/jquery/dist/jquery.min.js',
                    'node_modules/angular/angular.min.js',
                    'node_modules/angular-aria/angular-aria.min.js',
                    'node_modules/angular-animate/angular-animate.min.js',
                    'node_modules/angular-route/angular-route.min.js',
                    'node_modules/angular-sanitize/angular-sanitize.min.js',
                    'node_modules/angular-cookies/angular-cookies.min.js',
                    'node_modules/gsap/src/minified/TweenLite.min.js',
                    'node_modules/gsap/src/minified/plugins/ScrollToPlugin.min.js',
                    'js/external/three.min.js',
                    'js/external/photo-sphere-viewer.min.js'
                ],
                dest: 'libs.js'
            },
            jsctrl: {
                src: ['js/controllers/*.js'],
                dest: 'js/controllers.js'
            },
            jsserv: {
                src: ['js/services/*.js'],
                dest: 'js/services.js'
            },
            all: {
                options: {
                    stripBanners: true,
                    banner: '(function(){ ',
                    footer: '})();'
                },
                src: [
                    'js/start.js',
                    'js/services.js',
                    'js/controllers.js'
                ],
                dest: 'cus_script.js'
            },
            addlib: {
              src: ['libs.js', 'cus_script.js'],
                dest: 'script.js'
            }
        },
        watch: {
            css: {
                files: 'style/**/*.scss',
                tasks: ['sass'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: 'js/controllers/*.js',
                tasks: ['concat:jsctrl', 'concat:all', 'concat:addlib', 'uglify'],
                options: {
                    livereload: true
                }
            },
            js2: {
                files: 'js/services/*.js',
                tasks: ['concat:jsserv', 'concat:all', 'uglify'],
                options: {
                    livereload: true
                }
            }
        },
        sass: {
            options: {
                sourceMap: true,
                outputStyle: 'compressed'
            },
            dist: {
                files: {
                    'style.css': 'style/style.scss'
                }
            }
        },
        uglify: {
            options: {
                mangle: false,
                sourceMap: true
            },
            only: {
                files: {
                    'script.min.js': ['script.js']
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    // Default task(s).
    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'watch']);
};
